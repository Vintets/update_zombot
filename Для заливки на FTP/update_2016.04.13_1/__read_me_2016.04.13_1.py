
обновление 2016.04.13_1

game_actors_and_handlers\craft.py
добавляем крафт клея и железных сердец


в _mega_options:
в импорте вот такой теперь крафт
    from game_actors_and_handlers.craft import ExchangeHarvest, ExchangeFlyingShip,\
        ExchangeInstantrert, ExchangeShovelsBamboo, ExchangeShovelsNail,\
        ExchangeEmeraldObserv, ExchangeKleverhell, ExchangeChesnLiliy,\
        ExchangeHeliya, ExchangeTomatoskulls, ExchangeTube, Exchange1in1000,\
        Exchange1000in1, ExchangeEmeraldic, ExchangeBrains, ExchangeBolt,\
        ExchangeBabel, ExchangeRubber, ExchangeTransformator, ExchangeBlueHeart,\
        ExchangeFire, ExchangeFire2, ExchangeBoard, ExchangeKraska, ExchangeDiz,\
        ExchangeTermo, ExchangeLove1, ExchangeLove2, ExchangeSuperglue, ExchangeIronHeart
        
        
в выборе опций добавятся крафты:
                    ExchangeSuperglue,     # Cоздание супер-клея (в Ёлке)
                    ExchangeIronHeart,     # Создание железных сердец (в Вожде)

в таких местах
                    # ExchangeSuperglue,     # Cоздание супер-клея (в Ёлке)
                    ExchangeBolt,          # Создание болтов (в Эйфелевой)
                    ExchangeRubber,        # Создание резины (в останкино)
                    # ExchangeTransformator, # Создание трансформаторов (в башне)
                    # ExchangeIronHeart,     # Создание железных сердец (в Вожде)


соответственно в самих крафт опциях
                    'ExchangeSuperglue':{           # Cоздание супер-клея (в Ёлке)
                    'max_result':250,           # поддерживаем максимум супер-клея
                    'rezerv_1':1000,            # оставляем резерв желтой краски
                    'rezerv_2':100              # оставляем резерв трансформаторов
                    },
                    'ExchangeBlueHeart':{           # Создавать синие сердца (в Вожде)
                    'max_result':50,            # поддерживаем максимум синих сердец
                    'rezerv_1':1000,            # оставляем резерв Любви - красных сердец
                    'rezerv_2':100              # оставляем резерв трансформаторов
                    },

