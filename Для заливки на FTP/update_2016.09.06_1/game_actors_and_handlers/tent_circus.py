# coding=utf-8
import logging
from game_state.base import BaseActor
logger = logging.getLogger(__name__)


class BowReceiverBot(BaseActor):
    def perform_action(self):  # железные тыквы # бантики
        if self.if_location_pirate(): return
        bows = self._get_game_location().get_all_objects_by_type('halloweenTower')

        self.curuser = str(self._get_game_state().get_curuser())
        self.path_akkstat = self.mega().get_path_akkstat()
        self.path_akkstat_curuser = self.path_akkstat + self.curuser + '\\'
        if not hasattr(self._get_game_state(), 'airplane_user'):
            try:
                with open(self.path_akkstat_curuser + 'airplane_user.txt', 'r') as f:
                    self._get_game_state().airplane_user = eval(f.read())
            except:
                self._get_game_state().airplane_user = []

        self._event = []
        self.bow_count = 0
        for bow in bows:
            if bow.item == '@B_SAKURA': # '@B_MAUSOLEUM' '@B_TENT_CIRCUS'
                for i in bow.users:
                    if i.itemId != u'SAKURA_PACK_DEFAULT': continue # только бесплатные
                    self._get_game_state().airplane_user.append(i.id)
                    self._event.append({
                                        'extraId':i.id,
                                        'itemId':i.itemId,
                                        'action':'trick',
                                        'type':'item',
                                        'objId':bow.id
                                        })
                    name_user = self.addName(i.id)
                    sp_n = u' '*(14-len(name_user))
                    name = unicode(i.id).ljust(20, ' ') + name_user + sp_n
                    # if i.itemId == u'BOW_PACK_DEFAULT':
                        # count = 1
                    # elif i.itemId == u'BOW_PACK_SMALL':
                        # count = 3
                    # elif i.itemId == u'BOW_PACK_MEDIUM':
                        # count = 10
                    # if i.itemId == u'METALL_PACK_DEFAULT':
                        # count = 1
                    # elif i.itemId == u'METALL_PACK_SMALL':
                        # count = 5
                    # elif i.itemId == u'METALL_PACK_MEDIUM':
                        # count = 15
                    # elif i.itemId == u'METALL_PACK_LARGE':
                        # count = 30
                    if i.itemId == u'SAKURA_PACK_DEFAULT':
                        count = 1
                    elif i.itemId == u'SAKURA_PACK_SMALL':
                        count = 5
                    elif i.itemId == u'SAKURA_PACK_MEDIUM':
                        count = 15
                    elif i.itemId == u'SAKURA_PACK_LARGE':
                        count = 30 
                    # sms = u'Пользователь ' + name + u' положил Вишневый цвет: ' + unicode(count)
                    # logger.info(u'Пользователь %s положил Вишневый цвет: %d' % (name, count))
                    sms = u'Пользователь ' + name + u' положил Вишневый цвет: ' + unicode(count)
                    logger.info(u'Пользователь %s положил Вишневый цвет: %d' % (name, count))
                    with open(self.path_akkstat_curuser + 'action_frends.txt', 'a') as f:
                        sms += u'\n'
                        f.write(sms.encode('utf-8'))
                    self.bow_count += count
                    if len(self._event) > 499:
                        self.events_send()
                bow.users = []
                self.events_send()

    def events_send(self):
        if self._event != []:
            self._get_events_sender().send_game_events(self._event)
            # добавляем на склад
            # self._get_game_state().add_from_storage('@CR_153',self.bow_count)
            self._get_game_state().add_from_storage('@CR_198',self.bow_count)
            with open(self.path_akkstat_curuser + 'airplane_user.txt', 'w') as f:
                f.write(str(self._get_game_state().airplane_user))
            print
            if str(len(self._event))[-1:] == '1' and len(self._event) !=11:
                if str(self.bow_count)[-1:] == '1' and self.bow_count !=11:
                    logger.info(u'Собрали %d Вишневый цвет   от %d друга' % (self.bow_count, len(self._event)))
                elif 1 < int(str(self.bow_count)[-1:]) < 5 and self.bow_count < 5 and self.bow_count > 20:
                    logger.info(u'Собрали %d Вишневых цвета от %d друга' % (self.bow_count, len(self._event)))
                else:
                    logger.info(u'Собрали %d Вишневых цвета от %d друга' % (self.bow_count, len(self._event)))
            else:
                if str(self.bow_count)[-1:] == '1' and self.bow_count !=11:
                    logger.info(u'Собрали %d Вишневый цвет   от %d друзей' % (self.bow_count, len(self._event)))
                elif 1 < int(str(self.bow_count)[-1:]) < 5 and self.bow_count < 5 and self.bow_count > 20:
                    logger.info(u'Собрали %d Вишневых цвета от %d друзей' % (self.bow_count, len(self._event)))
                else:
                    logger.info(u'Собрали %d Вишневых цвета от %d друзей' % (self.bow_count, len(self._event)))
            self._event = []
            self.bow_count = 0
            
    def addName(self, id):
        if hasattr(self._get_game_state(), 'friends_names') and self._get_game_state().friends_names.get(id) and self._get_game_state().friends_names.get(id) != u'Без имени':
            name = u" '" + self._get_game_state().friends_names.get(id) + u"'"
            name = name.replace(u'\u0456', u'i').encode('UTF-8', 'ignore')
            name = unicode(name, 'UTF-8')
            #print name.replace(u'\u0456', u'i').encode('cp866', 'ignore')
        else: name = u''
        return name

"""
print (u'Собрали %(+4)d бантиков у %(+4)d друзей') % (20, 7)
u'remoteTrickTreating': [
{u'count': 0L, u'date': u'-14202657', u'user': u'17823741170892930057', u'item': u'@CR_153'}, 
{u'count': 0L, u'date': u'-14087056', u'user': u'11628291596603049922', u'item': u'@CR_153'},

           
{"type":"bellBox","id":"BOW_PACK_DEFAULT","name":"Бабочка","level":1,"buyCoins":0,"buyCash":0,"openCash":0,"gift":false,"xp":1,"shopImage":"crops/m_bow_tie.png","count":1,"item":"@CR_153","expire":43200,"disabled":false},
{"type":"bellBox","id":"BOW_PACK_SMALL","name":"3 Бабочки","level":1,"buyCoins":0,"buyCash":1,"openCash":0,"gift":true,"xp":1,"shopImage":"crops/m_bow_tie_3.png","count":3,"item":"@CR_153","disabled":false},
{"type":"bellBox","id":"BOW_PACK_MEDIUM","name":"10 Бабочек","level":1,"buyCoins":0,"buyCash":4,"openCash":0,"gift":true,"xp":1,"shopImage":"crops/m_bow_tie_5.png","count":10,"item":"@CR_153","disabled":false}

принять
{"extraId":"864730088123287177","itemId":"BOW_PACK_DEFAULT","action":"trick","type":"item","objId":24578}

FIND:  @B_TENT_CIRCUS   id =  24578
u'level': 0L, u'item': u'@B_TENT_CIRCUS', u'y': 70L, u'x': 14L, u'type': u'halloweenTower', u'id': 24578L, u'rotate': 0L, u'users':
{u'itemId': u'BOW_PACK_DEFAULT', u'expire': u'38559575', u'id': u'6064366433377019163'}, 
{u'itemId': u'BOW_PACK_DEFAULT', u'expire':u'38637626', u'id': u'7121282654775318802'}, 
{u'itemId': u'BOW_PACK_DEFAULT', u'expire': u'38832674', u'id': u'15818464092022489540'}, 
{u'itemId': u'BOW_PACK_DEFAULT', u'expire': u'38901416', u'id': u'14055452232076276468'}, 
{u'itemId': u'BOW_PACK_SMALL', u'id': u'4291172554993809160'}, 
{u'itemId': u'BOW_PACK_SMALL', u'id': u'4291172554993809160'}, 
""" 
