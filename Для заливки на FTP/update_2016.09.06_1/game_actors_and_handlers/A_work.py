# coding=utf-8
import pdb
import logging
import datetime
import random as random_number
import math
import time
from game_state.game_types import GameBuilding, GamePlayGame, DailyBonus, GamePlant
from game_state.base import BaseActor
from game_state.game_event import dict2obj, obj2dict
from game_actors_and_handlers.auto_pirat import AutoPirat


logger = logging.getLogger(__name__)

class Work(BaseActor):
    def perform_action(self):
        self.curuser = str(self._get_game_state().get_curuser())
        path_akkstat = self.mega().get_path_akkstat()
        self.path_akkstat_curuser = path_akkstat + self.curuser + '\\'
        #self.info_user('3689309721258147904')  #'10824204841001055531' #'3689309721258147904': # Винт-лист
        #self.Users_Info()
        #self.Soldier_Info()            # инфа об адмиралах
        #self.write_gameSTATE(self._get_game_state().get_state())    # вывод в файлы gameSTATE и gameSTATE_R
        #self.info_rects_object()       # вывод размера объекта
        # self.all_object_to_file()      # все объекты с острова в файл
        #self.print_data_level()        # вывод имени аккаунта + id
        #self.craft_test()              # тест крафта
        #self.storage2file_full()       # вывод склада в файл
        #self.storage2file()            # вывод склада фейков-пиратов
        #self.reload_LEMON()            # обновляем лимонные деревья
        return

        # for _obj in self._get_game_location().get_game_objects():
            # if 'SC_TRADER_GRAVE' in _obj.item:
                # print obj2dict(_obj)
                # break

        pdb.set_trace()
        raw_input('---------------   END   ---------------')
        return



        if self.if_location_pirate(): return
        if self._get_game_state().get_state().pirate.state == 'AWAY': return

        #treasureRehide = 404424467
        #treasureExpire = 21111647

        #location.guestInfos.userId  # кто у нас ходит в гостях


        # параметры друга
        """
        if True:
            print u'name            ', n.name
            print u'id              ', n.id
            print u'level           ', n.level
            print u'exp             ', n.exp
            print u'accessDate      ', n.accessDate
            print u'playerStatus    ', n.playerStatus
            print u'banned          ', n.banned
            #print u'liteGameState', n.liteGameState
            print u'Хотелка wishlist', n.liteGameState.wishlist
            print u'userName        ', n.liteGameState.playerSettings.userName
            print u'haveTreasure    ', n.liteGameState.haveTreasure
            print
        """

        # игра с параметрами объекта
        """
        if True:
            building = 'B_OBSERVATORY'
            for item in self._get_game_state().get_state().gameObjects:
                if (item.item == '@' + building):
                    print str(obj2dict(item))
                    print
                    d = dict2obj({u'Vnutr':u'555'}, 'MyAttr1')
                    setattr(item,u'MyAttr1',d)
                    setattr(item.nextPlayTimes,u'MyAttr2',u'myparam')
                    print type(item.nextPlayTimes)
                    print type(item.MyAttr1)
                    print item.MyAttr1.Vnutr
                    print
                    print str(obj2dict(item))
                    break
            raw_input('---------------   END   ---------------')

            building = 'B_OBSERVATORY'
            for build in self._get_game_state().get_state().gameObjects:
                if (build.item == '@' + building):
                    print str(obj2dict(build))
                    print
                    delayTime = 21600
                    craftId = u'OBSERVATORY_EMERALD_1'
                    if not hasattr(build,'nextPlayTimes'):
                        d = dict2obj({craftId:str(delayTime * 1000)}, 'nextPlayTimes')
                        setattr(build,'nextPlayTimes',d)
                    else:
                        setattr(build.nextPlayTimes,craftId,str(delayTime * 1000))
                    print
                    print str(obj2dict(build))
                    break
        """

        # вывод в файл инфы о крафтовом строении + инфа из items
        """
        if True:
            building = 'B_OBSERVATORY'
            for item in self._get_game_state().get_state().gameObjects:
                if (item.item == '@' + building):
                    print str(obj2dict(item))
                    open('craft.txt', 'a').write(str(obj2dict(item))+'\n'+'\n'.encode('utf-8'))
                    print
                    print self._get_item_reader().get(building).crafts
                    open('craft.txt', 'a').write(str(self._get_item_reader().get(building).crafts)+'\n'+'\n'.encode('utf-8'))

            raw_input('---------------   END   ---------------')
        """

        # инфа о шатре
        """
        if True:
            for object in self._get_game_location().get_game_objects():
                if object.item == '@B_TENT_CIRCUS':
                    print 'FIND: ', object.item, '  id = ', object.id
                    print obj2dict(object)
            raw_input('---------------   END   ---------------')
        """
        
        # деревья со сроком 1 день
        """
        if True:
            for object in self._get_game_location().get_game_objects():
                if '@FT_' in object.item and object.fruitingCount < 2:
                    print 'FIND: ', object.item, '  id = ', object.id
                    print obj2dict(object)
                if '@FT_PICKUP_BOX_' in object.item:
                    print 'FIND: ', object.item, '  id = ', object.id
                    print obj2dict(object)
            raw_input('---------------   END   ---------------')
        """

        # бесплатные лопаты
        """
        if True:
            u'remoteTreasure': [{u'count': 0L, u'date': u'-20040110', u'user': u'10748955583645013817'}, {u'count': 0L, u'date': u'-18012856', u'user': u'1998986519843446591'}, {u'count': 1L, u'date': u'-14664507', u'user': u'4291172554993809160'}, {u'count': 0L, u'date': u'-14084427', u'user': u'11628291596603049922'}, {u'count': 0L, u'date': u'-11624917', u'user': u'2933519876997138921'}, {u'count': 5L, u'date': u'-10556158', u'user': u'7900440316865515832'}, {u'count': 0L, u'date': u'-10252491', u'user': u'2785947119618774245'}]
        """

        # сколько золотых лопат
        """
        if True:
            storage = self._get_game_state().get_state().storageItems
            SHOVEL_EXTRA = 0
            for object in storage:
                print 'FIND: ', object.item #, '  id = ', object.id
                if object.item == '@SHOVEL_EXTRA':
                    shovel_extra = object.count
                    print 'FIND: ', object.item, '  count = ', shovel_extra
            raw_input('---------------   END   ---------------')
        """

        # вывод баф листа
        """
        if True:
            buff_list = self._get_game_state().get_state().buffs.list
            print str(obj2dict(buff_list))
            open('buff_list.txt', 'w').write(str(obj2dict(buff_list))+'\n'.encode('utf-8'))
            raw_input('---------------   END   ---------------')
        """

        # вывод данных о лимитах
        """
        if True:
            print 'haveRemoteFertilizeFruit ', self._get_game_state().get_state().haveRemoteFertilizeFruit  # поливать
            print 'remoteFertilizeFruitTree = ', len(self._get_game_state().get_state().remoteFertilizeFruitTree)
            print 'remoteMonsterPit = ', len(self._get_game_state().get_state().remoteMonsterPit)
            print 'remoteNewYear = ', len(self._get_game_state().get_state().remoteNewYear)
            print 'haveTreasure ', self._get_game_state().get_state().haveTreasure  # копать
            if hasattr(self._get_game_state(), 'gameObjects'):
                print 'gameObjects  Yes'
            
            if hasattr(self._get_game_state(), 'gameObjects'):
                for object in self._get_game_state().gameObjects:
                    if object.type == 'monsterPit':
                        print 'object.state = ', object.state
                    else:
                        print 'no monster'
                        break
            
            raw_input('---------------   END   ---------------')
        """

        # ищем объект на острове
        """
        for object in self._get_game_location().get_game_objects():
            print u'ищем...'
            if object.item[:4] == '@CH_':   
                print 'FIND: ', object.item, ' id = ', object.id
                print "Find ", str(obj2dict(object))
                raw_input('---------------   END   ---------------')
        """
        
        # Убираем ненужное
        """
        for object in self._get_game_location().get_game_objects():
            print u'ищем...'
            if object.item[:16] == '@PIRATE_CAPTURE_':   
                print 'FIND: ', object.item, ' id = ', object.id
                print "Find ", str(obj2dict(object)) 
                self._get_events_sender().send_game_events([{"type":"item","action":"moveToStorage","objId":object.id}])        
        raw_input('---------------   END   ---------------')
        """

        # сохраняем стату в файл
        #open('gameSTATE.txt', 'w').write(str(obj2dict(self._get_game_state())))
        
        # перрон в склад
        """
        #перрон в склад  {"type":"item","action":"moveToStorage","objId":14906}]}
        for object in self._get_game_location().get_game_objects():
            if hasattr(object, 'type'):
                if object.item == '@D_PLATFORM' or object.item == '@D_PLATFORM_2':
                    print 'steal perron ', object.item, ', id = ', object.id        
                    self._get_events_sender().send_game_events([{"type":"item","action":"moveToStorage","objId":object.id}])
        raw_input('---------------   END   ---------------')
        """

        """
        open_event={"x":97,"y":6,"locationId":"exploration_isle3_random","action":"move","type":"item","objId":-9452}
        self._get_events_sender().send_game_events([open_event])        
        """

        # базу в склад
        """
        for object in self._get_game_location().get_game_objects():
            if object.item == '@SC_BASE':
                print 'object.item ', object.item
                print 'object.id ', object.id
                self._get_events_sender().send_game_events([{"type":"item","action":"moveToStorage","objId":object.id}])         
        raw_input('---------------   END   ---------------')
        """

        # Сильверосклад
        """
        for object in self._get_game_location().get_game_objects():
            print u'ищем...'
            if object.item == '@PIRATE_ENEMY_1' or object.item == '@PIRATE_ENEMY_2' or object.item == '@PIRATE_ENEMY_3':   
                print 'FIND: ', object.item, 'id = ', object.id
                sms_perron = u'Name '+object.item+u'  id: '+unicode(object.id)+"\n"
                #open('zagadochniy_log.txt', 'a').write(sms_perron.encode("utf-8"))
                #find1 = object.id
                
                curloc = self._get_game_state().get_location_id()
                print u'Локация: ', curloc
                #open('zagadochniy_log.txt', 'a').write(curloc)
                
                self._get_events_sender().send_game_events([{"type":"item","action":"moveToStorage","objId":object.id}])        
        raw_input('---------------   END   ---------------')
        """        

        # Сильверонос
        """
        for object in self._get_game_location().get_game_objects():
            print u'ищем...'
            if object.item == '@PIRATE_ENEMY_1' or object.item == '@PIRATE_ENEMY_2'or object.item == '@PIRATE_ENEMY_3':   
                print 'FIND: ', object.item, 'id = ', object.id
                sms_perron = u'Name '+object.item+u'  id: '+unicode(object.id)+"\n"
                open('zagadochniy_log.txt', 'a').write(sms_perron.encode("utf-8"))
                find1 = object.id
                
                curloc = self._get_game_state().get_location_id()
                print u'Локация: ', curloc
                open('zagadochniy_log.txt', 'a').write(curloc)
                
                # переходим
                logger.info(u'Переходим на домашний ')
                change_location_event = {
                            "user": None,
                            "locationId" : u'main',
                            "type":"gameState",
                            "action":"gameState",
                            "objId": None
                            }
                self._get_events_sender().send_game_events([change_location_event])
                open_event={"x":97,"y":6,"locationId":curloc,"action":"move","type":"item","objId":find1}
                self._get_events_sender().send_game_events([open_event])
                raw_input('---------------   END   ---------------')
        """

        # Берём всё, кроме колодцев, труб и крюков.
        """
        for object in self._get_game_location().get_game_objects():
            if object.item == '@B_STONE_WELL' or object.item == '@WEALTH_SPYGLASS' or object.item == '@WEALTH_VALBEAR' or object.item == '@WEALTH_HOOK':
                continue
            print 'FIND: ', object.item, 'id = ', object.id
            self._get_events_sender().send_game_events([{"type":"item","action":"moveToStorage","objId":object.id}])
            self._get_game_location().remove_object_by_id(object.id)

        # @WEALTH_SPYGLASS
        # @WEALTH_VALBEAR
        # @WEALTH_HOOK

        raw_input('---------------   END   ---------------')
        """

        #Рулетка с загадочного анализ
        """
        if True:
            for object in self._get_game_location().get_game_objects():
                print 'object.item ', object.item
                print 'object.id ', object.id
                sms_perron = u'Name '+object.item+u'  id: '+unicode(object.id)+"\n"
                open('zagadochniy_log.txt', 'a').write(sms_perron.encode("utf-8"))
            
                
                if object.item == '@B_MAST':   
                    print 'FIND: ', object.id
                    __find1 = object.id
                    
            current_loc_id = self._get_game_state().get_location_id()
            print current_loc_id
            open('zagadochniy_log.txt', 'a').write(current_loc_id)
            raw_input('---------------   END   ---------------')
        """       

        #Университет с тропического анализ
        """
        if True:
            #{"events":[{"x":77,"y":84,"locationId":"exploration_tropic2","action":"move","type":"item","objId":541}]}
            #next_id = 0
            #for object in self._get_game_location().get_game_objects():
            #    if object.id > next_id: next_id = object.id
            #next_id += 1
                
            for object in self._get_game_location().get_game_objects():
                print 'object.item ', object.item
                print 'object.id ', object.id
                if object.item == '@B_UNIVERCITY':
                    print 'FIND: ', object.id
                    __find1 = object.id
                if object.item == '@B_MAST':   
                    print 'FIND: ', object.id
                    __find2 = object.id
                if object.item == '@B_OIL':   
                    print 'FIND: ', object.id
                    __find3 = object.id 
            
            # переходим
            logger.info(u'Переходим на домашний ')
            change_location_event = {
                    "user": None,
                    "locationId" : u'main',
                    "type":"gameState",
                    "action":"gameState",
                    "objId": None
                    }
            self._get_events_sender().send_game_events([change_location_event])

            # ставим на домашний
            #open_event={"x":77,"y":84,"locationId":"exploration_sweet1","action":"move","type":"item","objId":-10224}
            #self._get_events_sender().send_game_events([open_event])
            
            open_event={"x":97,"y":6,"locationId":"exploration_sweet1","action":"move","type":"item","objId":-10224}
            self._get_events_sender().send_game_events([open_event])

            open_event={"x":97,"y":8,"locationId":"exploration_sweet1","action":"move","type":"item","objId":-10225}
            self._get_events_sender().send_game_events([open_event])
            raw_input('---------------   END   ---------------')

            # тропический2 exploration_tropic2
            # сладкий exploration_sweet1
            # events:[{"x":97,"y":6,"locationId":"isle_omega","action":"move","type":"item","objId":-8267}] 
        """

        """
        open_event = {"x":97,"y":6,"locationId":'exploration_isle2_random',"action":"move","type":"item","objId":-9304}
        self._get_events_sender().send_game_events([open_event])
        """

        # тянем домой стадион
        """
        if True:
            # Ищем
            loc = self._get_game_state().get_game_loc().get_location_id()
            for object in self._get_game_location().get_game_objects():
                if object.item == '@B_STADIUM':
                    print 'FIND: ', object.id
                    __find1 = object.id

            # переходим на домашний
            # ставим
            open_event={"x":97,"y":6,"locationId":"exploration_sweet1","action":"move","type":"item","objId":-10224}
            self._get_events_sender().send_game_events([open_event])
        """
 
        # балуемся с туковыми башнями
        """
        for object in self._get_game_location().get_game_objects():
            print u'ищем...'
            if object.item == '@туковае имя':
                print 'FIND: ', object.item, ' id = ', object.id
                
                storage = self._get_game_state().get_state().storageGameObjects
                for _obj in storage:
                    if _obj.item == '@имя выставляемой':
                        event = {
                            "x":20,
                            "y":20,
                            "action":"placeFromStorage",
                            "itemId":_obj.item[1:],
                            "type":"item",
                            "objId":object.id
                            } 
                        self._get_events_sender().send_game_events([event])
                        raw_input('---------------   END   ---------------')
                        exit(0)
                        #B_HALLOWEEN
                        #SYMBOL_D_BOX  sell_track = {"action":"pick","type":"item","objId":object.id
        """

        # смотрим лимит ангаров с проверкой уровня постройки
        """
        for obj in self._get_game_state().get_state().gameObjects:
            if '_SKLAD_' in obj.item:
                print obj.level
                reader = self._get_item_reader().get(obj.item).giftCoinses[0]
                print reader, type(reader)
                print reader.level
                print reader.count
                if obj.level == reader.level:
                    print u'Прибавляем', reader.count

                # "id":"B_SKLAD_COINS"
                # "giftCoinses":[{"level":3,"count":5000}]
        """

        # человек
        """
         {u'isAway': False, 
         u'haveTreasure': False, 
         u'haveAttempts': True, 
         u'haveRemoteFertilizeFruit': True, 
         u'gameObjects': [], 
         u'ownGameObjects': [], 
         u'location': {u'width': 128L, u'openedAreas': [u'mount', u'second'], u'height': 128L, u'item': u'@main', u'guestInfos': [], u'type': u'location', u'id': u'main'}, 
         u'playerSettings': {u'userName': u'1200', 
         u'dressId': u'CL_WO_COSTUME', 
         u'hatId': u'CL_WO_HEAD'}, 
         u'action': u'gameState', 
         u'playerStatus': u'@PS_HUMAN', 
         u'treasureRehide': 332100794L, 
         u'type': u'gameState', 
         u'wishlist': [], 
         u'haveThanksgivingAttempt': True}
        """

        # friends
        """
        if True:
            events = {"type":"players","id":3,"action":"getInfo","players":["3689309721258149862","3689309721258147904"]} 
            self._get_events_sender().send_game_events([events])
            self._get_game().handle_all_events()
            time.sleep(2)
            self._get_game().handle_all_events()
            getInfo = self._get_game_state().playersInfo
            #print obj2dict(getInfo)
            #print dir(players)
            #print
            for n in getInfo:
                print u'name            ', n.name
                print u'id              ', n.id
                print u'level           ', n.level
                print u'exp             ', n.exp
                print u'accessDate      ', n.accessDate
                print u'playerStatus    ', n.playerStatus
                print u'banned          ', n.banned
                #print u'liteGameState', n.liteGameState
                print u'Хотелка wishlist', n.liteGameState.wishlist
                print u'userName        ', n.liteGameState.playerSettings.userName
                print u'haveTreasure    ', n.liteGameState.haveTreasure
                print
            #open('getInfo.txt', 'a').write(str(getInfo)+"\n".encode('utf-8'))
            raw_input('---------------   END   ---------------')
        """

        # вскрываем букву D
        """
        if True:
            for object in self._get_game_location().get_game_objects():
                if object.item == '@SYMBOL_D_BOX':   
                    print 'FIND: ', object.item, ' id = ', object.id
                    event = {"action":"pick","type":"item","objId":object.id}
                    self._get_events_sender().send_game_events([event])
                    storage = self._get_game_state().get_state().storageGameObjects
                    for _obj in storage:
                        # if _obj.item == '@SYMBOL_D_BOX':
                            # event = {
                                # "x":14,
                                # "y":12,
                                # "action":"placeFromStorage",
                                # "itemId":_obj.item[1:],
                                # "type":"item",
                                # "objId":object.id
                                # } 
                            # self._get_events_sender().send_game_events([event])               
            raw_input('---------------   END   ---------------')
            exit(0)
        """ 

        # Инфа о друзьях
        if True:
            """
            # всю инфу в файл
            if hasattr(self._get_game_state(), 'playersInfo'):
                for n in self._get_game_state().playersInfo:
                    open('getInfo.txt', 'a').write(str(n)+"\n"+"\n".encode('utf-8'))
            raw_input('---------------   END   ---------------')


            # friends
            events = {"type":"players","id":3,"action":"getInfo","players":["3689309721258149862","3689309721258147904"]} 
            self._get_events_sender().send_game_events([events])
            self._get_game().handle_all_events()
            time.sleep(2)
            self._get_game().handle_all_events()
            getInfo = self._get_game_state().evinf
            #print obj2dict(getInfo)
            #print dir(players)
            #print players
            #print
            for n in getInfo.players:
                print u'name            ', n.name
                print u'id              ', n.id
                print u'level           ', n.level
                print u'exp             ', n.exp
                print u'accessDate      ', n.accessDate
                print u'playerStatus    ', n.playerStatus
                print u'banned          ', n.banned
                #print u'liteGameState', n.liteGameState
                print u'Хотелка wishlist', n.liteGameState.wishlist
                print u'userName        ', n.liteGameState.playerSettings.userName
                print u'haveTreasure    ', n.liteGameState.haveTreasure
                print
            #open('getInfo.txt', 'a').write(str(getInfo)+"\n".encode('utf-8'))
            raw_input('---------------   END   ---------------')
            """

            """
            # friends
            events = {"type":"players","id":2,"action":"getInfo","players":["8997900496038913535"]}
            self._get_events_sender().send_game_events([events])
            print u'Отсылаем запрос 1'
            self._get_game().handle_all_events()
            time.sleep(2)
            self._get_game().handle_all_events()
            self.printInfo()
            events = {"type":"players","id":3,"action":"getInfo","players":["3689309721258149862","3689309721258147904"]} 
            self._get_events_sender().send_game_events([events])
            print u'Отсылаем запрос 2'
            self._get_game().handle_all_events()
            time.sleep(2)
            self._get_game().handle_all_events()
            self.printInfo()
            raw_input('---------------   END   ---------------')
            """

            """
            name             Винт-лист
            id               3689309721258147904
            level            46
            exp              839329
            accessDate       -1323582
            playerStatus     @PS_ZOMBIE
            banned           False
            Хотелка wishlist [u'@CR_06', None, None, u'@S_10']
            userName         Винт-лист
            haveTreasure     True

            u'buried': u'11094806613916288513'
            u'makeup': u'girl_makeup'   !!!!!

            name             Vi-Inb
            id               3689309721258149862
            level            47
            exp              973760
            accessDate       -62508523
            playerStatus     @PS_ZOMBIE
            banned           False
            Хотелка wishlist [None, None, None, u'@R_26']
            userName         Vi-Inb
            haveTreasure     True
            """

            """
            if True:
                [GamePlayer: {u'achievements': [u'@ACHIEV_03', u'@ACHIEV_09', u'@ACHIEV_10', u'@ACHIEV_12', u'@ACHIEV_14', u'@ACHIEV_15', u'@ACHIEV_17', u'@ACHIEV_18', u'@ACHIEV_19', u'@ACHIEV_21', u'@ACHIEV_22', u'@ACHIEV_26', u'@ACHIEV_28', u'@ACHIEV_31', u'@ACHIEV_34', u'@ACHIEV_36', u'@ACHIEV_40', u'@ACHIEV_41', u'@ACHIEV_42', u'@ACHIEV_47'], u'name': u'\u0412\u0438\u043d\u0442-\u043b\u0438\u0441\u0442', u'level': 46L, u'accessDate': u'-57947750', u'banned': False, u'exp': 837509L, u'playerStatus': u'@PS_ZOMBIE', u'statusByCountedItems': [], u'liteGameState': GameLiteGameState: {u'wishlist': [u'@CR_06', None, None, u'@S_10'], u'playerSettings': GamePlayerSettings: {u'userName': u'\u0412\u0438\u043d\u0442-\u043b\u0438\u0441\u0442', u'dressId': u'CL_MATROSKA_DEF', u'hatId': u'CL_BUDENOVKA_DEF'}, u'haveTreasure': True}, u'id': u'3689309721258147904'}, GamePlayer: {u'achievements': [u'@ACHIEV_03', u'@ACHIEV_04', u'@ACHIEV_05', u'@ACHIEV_09', u'@ACHIEV_10', u'@ACHIEV_12', u'@ACHIEV_14', u'@ACHIEV_15', u'@ACHIEV_17', u'@ACHIEV_18', u'@ACHIEV_19', u'@ACHIEV_21', u'@ACHIEV_22', u'@ACHIEV_26', u'@ACHIEV_28', u'@ACHIEV_31',u'@ACHIEV_34', u'@ACHIEV_36', u'@ACHIEV_40', u'@ACHIEV_41', u'@ACHIEV_42', u'@ACHIEV_44', u'@ACHIEV_47'], u'name': u'Vi-Inb', u'level': 47L, u'accessDate': u'-57943923', u'banned': False, u'exp': 973760L, u'playerStatus': u'@PS_ZOMBIE', u'statusByCountedItems': [], u'liteGameState': GameLiteGameState: {u'wishlist': [None, None, None, u'@R_26'], u'playerSettings': GamePlayerSettings: {u'userName':u'Vi-Inb', u'dressId': u'CL_SHIRT01_DEF', u'hatId': u'CL_PIRATEHAT_DEF'}, u'haveTreasure': True}, u'id': u'3689309721258149862'}]

                {u'achievements': [u'@ACHIEV_01', u'@ACHIEV_02', u'@ACHIEV_03', u'@ACHIEV_05', u'@ACHIEV_06', u'@ACHIEV_08', u'@ACHIEV_09', u'@ACHIEV_10', u'@ACHIEV_11', u'@ACHIEV_12', u'@ACHIEV_13', u'@ACHIEV_14', u'@ACHIEV_15', u'@ACHIEV_16', u'@ACHIEV_17', u'@ACHIEV_18', u'@ACHIEV_19', u'@ACHIEV_20', u'@ACHIEV_21', u'@ACHIEV_22', u'@ACHIEV_24', u'@ACHIEV_25', u'@ACHIEV_26', u'@ACHIEV_28', u'@ACHIEV_29', u'@ACHIEV_30', u'@ACHIEV_31', u'@ACHIEV_32', u'@ACHIEV_34', u'@ACHIEV_35', u'@ACHIEV_36', u'@ACHIEV_38', u'@ACHIEV_39', u'@ACHIEV_40', u'@ACHIEV_41', u'@ACHIEV_42', u'@ACHIEV_43', u'@ACHIEV_44', u'@ACHIEV_46', u'@ACHIEV_47'], u'name': u'\u0437\u043e\u043c\u0431\u0456 49', u'level': 49L, u'accessDate': u'-2695632403', u'banned': False, u'exp': 1216714L, u'playerStatus': u'@PS_ZOMBIE', u'statusByCountedItems': [], u'liteGameState': GameLiteGameState: {u'wishlist': [None, None, None,u'@CR_55'], u'playerSettings': GamePlayerSettings: {u'userName': u'\u0437\u043e\u043c\u0431\u0456 49', u'makeup': u'girl_makeup', u'dressId': u'CL_DRESS_WITCH_4', u'hatId': u'CL_HAIR02_DEF'}, u'haveTreasure': True}, u'id': u'6547313103724221540'}

                u'buried': u'11094806613916288513'
                u'makeup': u'girl_makeup'
            """

            """
            if True:
                u'action', u'id', u'players', u'type'

                u'action': u'getInfo',
                u'type': u'playersInfo',
                u'id': u'4' 
                u'players': [{
                u'achievements': [u'@ACHIEV_03', u'@ACHIEV_04', u'@ACHIEV_05', u'@ACHIEV_09', u'@ACHIEV_10', u'@ACHIEV_12', u'@ACHIEV_14', u'@ACHIEV_15', u'@ACHIEV_17', u'@ACHIEV_18', u'@ACHIEV_19', u'@ACHIEV_21', u'@ACHIEV_22', u'@ACHIEV_26', u'@ACHIEV_28', u'@ACHIEV_31', u'@ACHIEV_34', u'@ACHIEV_36', u'@ACHIEV_40', u'@ACHIEV_41', u'@ACHIEV_42', u'@ACHIEV_44', u'@ACHIEV_47'], 
                u'name': u'Vi-Inb', 
                u'level': 46L, 
                u'accessDate': u'-6259405', 
                u'banned': False, 
                u'exp': 830719L, 
                u'playerStatus': u'@PS_ZOMBIE', 
                u'statusByCountedItems': [], 
                u'liteGameState': {u'wishlist': [u'@R_24', u'@CR_36', u'@CR_68', u'@R_02'], u'playerSettings': {u'userName': u'Vi-Inb', u'dressId': u'CL_SHIRT01_DEF', u'hatId': u'CL_PIRATEHAT_DEF'}, u'haveTreasure': False}, 
                u'id': u'3689309721258149862'
                }]

                File "C:\Python27\lib\encodings\cp866.py", line 12, in encode return codecs.charmap_encode(input,errors,encoding_map) 
                UnicodeEncodeError: 'charmap' codec can't encode character u'\u0456' in positio 6: character maps to <undefined>

                a = u'\u2122\u2020\u0410\u0440\u0442\u0451\u043c\u2020\u2122(31'
                ™†Артём†™(31

                'Без имени'
                '\u0411\u0435\u0437 \u0438\u043c\u0435\u043d\u0438'
            """

        # дарение бесплатки
        """
        if True:
            SMS = u'Всем добра'  # сообщение к подарку
            # создаём список с бесплатками
            freeGifts = []
            for i in self._get_item_reader().get("FREE_GIFTS").freeGifts:
                freeGifts.append(i.item)
            #print 'freeGifts', freeGifts

            # создаём список уже осчастливленных
            GU_go = []
            for i in self._get_game_state().get_state().freeGiftUsers:
                GU_go.append(i.user)
            print obj2dict(self._get_game_state().get_state().freeGiftUsers)
            #print len(GU_go)

            self._event = []
            for playerInfo in self._get_game_state().playersInfo:
                if playerInfo.id in GU_go: continue
                wish = []
                for w in playerInfo.liteGameState.wishlist:
                    if w != None and (w in freeGifts): wish.append(w)
                if not wish:
                    wish.append('@CR_16')
                self._event.append({"userIds":[str(playerInfo.id)],"msg":SMS,"type":"gifts","action":"sendFreeGifts","itemId":random_number.choice(wish)[1:]})
                self._get_game_state().get_state().freeGiftUsers.append(dict2obj({u'blockedUntil': u'86400000', u'user': playerInfo.id}))
                if len(self._event) > 499:
                    self.events_send()
            self.events_send()
        """

        # пират? когда плавали
        """
        print self._get_game_state().get_state().pirate.state
        print self._get_game_state().get_state().pirate.sailingDate
        """

    # таверна
    """
    if True:
        State CITIZEN RETIRNED
        u'subType': u'taverna', 
        u'objType': 7L, 
        u'xp': 100L, 
        u'id': u'B_TAVERNA', 
        u'level': 3L, 
        u'games': [{
        u'requirements': [{u'requirements': [{u'item': u'@PS_PRISONER', u'type': u'playerStatusItemRequirement'}], u'type': u'not'}, {u'requirements': [{u'type': u'hasStorageComposition', u'composition': u'@PIRATE_BOX'}], u'type': u'not'}, {u'requirements': [{u'type': u'hasStorageComposition', u'composition': u'@PIRATE_BOX_2'}], u'type': u'not'}, {u'requirements': [{u'type': u'pirateStateRequirement'}, {u'state': u'CITIZEN', u'type': u'pirateStateRequirement'}, {u'state': u'RETURNED', u'type': u'pirateStateRequirement'}, {u'state': u'DEAD', u'type': u'pirateStateRequirement'}], u'type': u'or'}], 

        u'unconditionalPlayCost': {u'count': 5L, u'item': u'@DUBLON'}, 
        u'hint': u'\u0427\u0442\u043e\u0431\u044b \u0441\u0442\u0430\u0442\u044c \u041f\u0438\u0440\u0430\u0442\u043e\u043c, \u0432\u044b\u0438\u0433\u0440\u0430\u0439 \u0421\u0443\u043d\u0434\u0443\u043a \u041f\u0438\u0440\u0430\u0442\u0430 \u0432 \u0440\u0443\u043b\u0435\u0442\u043a\u0435 \u0438 \u043f\u043e\u0441\u0442\u0430\u0432\u044c \u0435\u0433\u043e \u043d\u0430 \u043b\u044e\u0431\u043e\u0439 \u043e\u0441\u0442\u0440\u043e\u0432.', 
        u'delayTime': 86400L, 
        u'prizes': [{u'count': 1L, u'item': u'@DUBLON'}, {u'count': 1L, u'item': u'@PIRATE_BOX'}, {u'count': 5L, u'item': u'@CHOP_MACHETE'}, {u'count': 1L, u'item': u'@PIRATE_BOX'}, {u'count': 5L, u'item': u'@CHOP_HAMMER'}, {u'count': 1L, u'item': u'@DUBLON'}, {u'count': 5L, u'item': u'@CHOP_MACHETE'}, {u'count': 1L, u'item': u'@PIRATE_BOX'}, {u'count': 5L, u'item': u'@CHOP_AXE'}], 
        u'level': 3L, u'type': u'roulette', u'id': u'B_TAVERNA_ROULETTE_1', u'bonusPrize': {u'count': 1L, u'item': u'@PIRATE_STATE_CITIZEN'}
        }]
        return
    """

    def print_data_level(self):  # вывод имени аккаунта + id
        my_id = self._get_game_state().get_my_id()
        print 'curuser = ', self.curuser, my_id

    def all_object_to_file(self):  # все объекты с острова в файл
        for object in self._get_game_location().get_game_objects():
            print 'FIND: ', object.item.ljust(27, " "), ' id = ', object.id
            sms = u'Name ' + object.item.ljust(27, " ") + u' id: ' + unicode(object.id)+'\n'
            open('object_pirat.txt', 'a').write(sms.encode("utf-8"))
            open('object_pirat.txt', 'a').write(str(obj2dict(object))+'\n'+'\n'.encode('utf-8'))
        raw_input('---------------   END   ---------------')

    def info_rects_object(self, user):
        for obj in self._get_game_state().get_state().rectsObjects:
            if 'b_thanksgiving_horn_' in str(obj.objAnim):
                #need.w = int(obj.rects.rectW)
                #need.h = int(obj.rects.rectH)
                print str(obj2dict(obj))
        raw_input('---------------   END   ---------------')
        #{'rects': {'rectH': '3', 'rectY': '0', 'rectX': '0', 'rectW': '4'}, 'objAnim': 'b_thanksgiving_horn_01'}
        #{'rects': {'rectH': '5', 'rectY': '0', 'rectX': '0', 'rectW': '5'}, 'objAnim': 'b_thanksgiving_horn_02'}
        #{'rects': {'rectH': '5', 'rectY': '0', 'rectX': '0', 'rectW': '6'}, 'objAnim': 'b_thanksgiving_horn_03'}
        #{'rects': {'rectH': '4', 'rectY': '0', 'rectX': '0', 'rectW': '4'}, 'objAnim': 'antizombie_01'}
        pass

    def info_user(self, user):
        if not hasattr(self._get_game_state(), 'playersInfo'): return
        players_info = self._get_game_state().playersInfo
        print 'playersInfo:', len(players_info)
        for n in players_info:
            if str(n.id) == user:
                #print dir(n)
                self.target = open(self.path_akkstat_curuser + 'UserInfo.txt', 'a')
                gameSTATE = obj2dict(n)
                #self.target.write(str(n)+"\n"+"\n")
                self.target.write(str(gameSTATE)+"\n"+"\n")

                print '---All len---', len(gameSTATE.keys())
                self.target.write('---All len---'+str(len(gameSTATE.keys()))+"\n"+"\n")
                self.print_data_level(gameSTATE, 0, True)
                self.target.write(u'\n'+u'================================================================'+u'\n'+u'\n')
                self.target.close()

                """
                print u'name            ', n.name
                print u'id              ', n.id
                print u'level           ', n.level
                print u'exp             ', n.exp
                print u'accessDate      ', n.accessDate
                print u'playerStatus    ', n.playerStatus
                print u'banned          ', n.banned
                #print u'liteGameState', n.liteGameState
                print u'Хотелка wishlist', n.liteGameState.wishlist
                print u'userName        ', n.liteGameState.playerSettings.userName
                print u'haveTreasure    ', n.liteGameState.haveTreasure
                print
                """

                raw_input('---------------   END   ---------------')
                break

    def print_data_level(self, data, num=0, pr=False):
        if type(data) == dict:
            for el in data.keys():
                if type(data[el]) == int or type(data[el]) == float or type(data[el]) == long or type(data[el]) == str or type(data[el]) == bool:
                    if pr: print ' '*num*8+str(el).ljust(12, " ")+' = '+str(data[el])
                    text = ' '*num*8 + str(el).ljust(12, " ") + ' = ' + str(data[el]) +"\n"
                    self.target.write(text.encode("utf-8"))
                elif type(data[el]) == unicode:
                    if pr: print ' '*num*8+str(el).ljust(12, " ")+' = '+data[el]
                    text = ' '*num*8+str(el).ljust(12, " ")+' = '+data[el]+"\n"
                    self.target.write(text.encode("utf-8"))
                elif type(data[el]) == dict or type(data[el]) == list:
                    space = ' '*(21-len(str(el)))
                    if pr: print ' '*num*8+str(str(el)+space+' len '+str(len(data[el]))).ljust(31, " ")+str(type(data[el]))
                    text = ' '*num*8+str(str(el)+space+' len '+str(len(data[el]))).ljust(31, " ")+str(type(data[el])) +"\n"
                    self.target.write(text.encode("utf-8"))
                    self.print_data_level(data[el], num+1, pr)
                else:
                    if pr: print 'unknown type'
                    text = 'unknown type' +"\n"
                    self.target.write(text.encode("utf-8"))
        elif type(data) == list:
            for el in data:
                if type(el) == int or type(el) == float or type(el) == long or type(el) == str or type(el) == bool:
                    if pr: print ' '*num*8+str(el)
                    text = ' '*num*8+str(el) +"\n"
                    self.target.write(text.encode("utf-8"))
                elif type(el) == unicode:
                    if pr: print ' '*num*8+str(el)
                    text = ' '*num*8+str(el) +"\n"
                    self.target.write(text.encode("utf-8"))
                elif type(el) == dict:
                    space = ' '*(21-len(el))
                    if pr: print ' '*num*8+str(type(el))+' len '+str(len(el))
                    text = ' '*num*8+str(type(el))+' len '+str(len(el)) +"\n"
                    self.target.write(text.encode("utf-8"))
                    self.print_data_level(el, num+1, pr)
                elif type(el) == list:
                    space = ' '*(21-len(el))
                    if pr: print ' '*num*8+str(type(el))+' len '+str(len(el))
                    text = ' '*num*8+str(type(el))+' len '+str(len(el)) +"\n"
                    self.target.write(text.encode("utf-8"))
                    self.print_data_level(el, num+1, pr)
                else:
                    if pr: print 'unknown type'
                    text = 'unknown type' +"\n"
                    self.target.write(text.encode("utf-8"))
        else:
            if pr: print 'unknown type 0'
            text = 'unknown type 0' +"\n"
            self.target.write(text.encode("utf-8"))
        #self.target.write(u"\n")

    def count_in_storage(self, item_id):
        if os.path.isfile('storage.txt'): exif = 1
        else: exif = 0
        for itemid in self.__game_state.storageItems:
            if hasattr(itemid, "item"): 
                if itemid.item == item_id: ret = itemid.count
                if exif == 0:
                    #if itemid.item[:3] == "@S_":open('storage.txt', 'a').write(itemid.item.replace('@S_', 'P_')+": "+str(itemid.count)+"\n")
                    try:
                        name = self.__item_reader.get('P_'+itemid.item[3:]).name
                    except:
                        name = "N/A"
                    open('storage.txt', 'a').write((name+": "+str(itemid.count)+"\t\t"+itemid.item+"\n").encode('utf-8', 'ignore'))
                    #-------------------------------------
                #if itemid.item == item_id: ret = itemid.count
        try: return ret
        #print "##################################################"
        except: return 0

    def events_send(self):
        if self._event != []:
            self._get_events_sender().send_game_events(self._event)
            if int(len(self._event)[-1:]) == 1:
                logger.info(u'Отослали бесплатку %s другу' % (len(self._event)))
            else:
                logger.info(u'Отослали бесплатку %s друзьям' % (len(self._event)))
            self._event = []
        
        #GameFreeGiftUser: {u'blockedUntil': u'29677878', u'user': u'10831211684864283025'}
        # u'25228781'
        # if len(wishlst) > 0:
            # slctwish = random_number.choice(wishlst)#тут по хотелочке случайный выбор
        # else: slctwish = random_number.choice(['@CR_70', '@CR_44', '@CR_25', '@CR_16', '@CR_06'])#тут если нет хотелочек случайный выбор
        pass

    def Soldier_Info(self):  # инфа об адмиралах
        for object in self._get_game_location().get_game_objects():
            #print u'ищем...'
            if object.item == '@B_SOLDIER':
                print 'FIND: ', object.item, ' id = ', object.id
                print "Find ", str(obj2dict(object))
                self.target = open('Soldier.txt', 'a')
                gameSTATE = obj2dict(object)
                self.target.write(str(gameSTATE)+"\n"+"\n")

                print '---All len---', len(gameSTATE.keys())
                self.target.write('---All len---'+str(len(gameSTATE.keys()))+"\n")
                self.print_data_level(gameSTATE, 0, True)
                print
                print
                self.target.write("\n"+"\n")
        raw_input('---------------   END   ---------------')

    def write_gameSTATE(self, gameSTATE):
        open(self.path_akkstat_curuser + 'gameSTATE.txt', 'w').write(str(obj2dict(gameSTATE)))
        gameSTATE = obj2dict(gameSTATE)
        self.target = open(self.path_akkstat_curuser + 'gameSTATE_R.txt', 'w')
        print '---All len---', len(gameSTATE.keys())
        self.target.write('---All len---'+str(len(gameSTATE.keys()))+"\n"+"\n")
        self.target.close()
        self.target = open(self.path_akkstat_curuser + 'gameSTATE_R.txt', 'a')
        self.print_data_level(gameSTATE, 0, False)
        self.target.write(u'\n'+u'================================================================'+u'\n'+u'\n')
        self.target.close()
        raw_input('---------------   END   ---------------')

    def Users_Info(self):
        if not hasattr(self._get_game_state(), 'playersInfo'): return
        players_info = self._get_game_state().playersInfo

        self.target = open('Users_Info.txt', 'w')
        print u'All playersInfo: ', len(players_info)
        self.target.write(u'All playersInfo: '+str(len(players_info))+"\n"+"\n")
        print u'id\tName\tLevel\tExp\tStatus\tBanned\tAccessDate\tWishList'
        self.target.write(u'id\tName\tLevel\tExp\tStatus\tBanned\tAccessDate\tWishList'+"\n")
        self.target.close()
        
        self.target = open('Users_Info.txt', 'a')
        for n in players_info:
            accessDate = ''
            if hasattr(n, 'accessDate'): accessDate = n.accessDate
            name = ''
            if hasattr(n, 'name'): name = n.name
            name = name.replace(u'\u0456', u'i')
            text = n.id + u'\t' + name + u'\t' + str(n.level) + u'\t' + str(n.exp) + u'\t' + n.playerStatus + u'\t' + str(n.banned) + u'\t' + accessDate + u'\t' + str(n.liteGameState.wishlist) + u'\n'
            print text.encode("cp866", "ignore")  # "UTF-8"
            self.target.write(text.encode("UTF-8", "ignore"))

        raw_input('---------------   END   ---------------')

    # Обновляем лимон
    def reload_LEMON(self):
        loc = self._get_game_state().get_game_loc().get_location_id()
        events = []
        for object in self._get_game_location().get_game_objects():
            if object.item == '@FT_LEMON'and object.fruitingCount < 3:
                id = object.id
                x = object.x
                y = object.y
                print u'Обновляем лимонное дерево: ', '  id = ', id, '  x/y: ', x, '/', y, u'  кручено ', object.fruitingCount
                #print obj2dict(object)
                event_to = {"type":"item","action":"moveToStorage","objId":id}
                event_from = {"x":x, "y":y, "action":"placeFromStorage", "itemId":'FT_LEMON', "type":"item", "objId":id}
                events.append(event_to)
                events.append(event_from)
        if len(events) > 0:
            self._get_events_sender().send_game_events(events)
                
        # self._get_events_sender().send_game_events([{"type":"item","action":"moveToStorage","objId":id}])
        # event = {"x":x, "y":y, "action":"placeFromStorage", "itemId":'FT_LEMON', "type":"item", "objId":id}
        # self._get_events_sender().send_game_events([event])

        #FIND:  @FT_LEMON   id =  42784
        #{u'rotate': 0L, u'fruitingCount': 24L, u'fertilized': False, u'item': u'@FT_LEMON', u'jobFinishTime': u'172689319', u'jobStartTime': u'-110681', u'y': 34L, u'x': 33L, u'type': u'fruitTree', u'id': 42784L}
        pass

    def craft_test(self):  # тест крафта
        options = {
                'max_result':3527050,   # поддерживаем максимум золотых лопат
                'rezerv_1':62440,       # оставляем резерв бамбука
                'rezerv_2':1740000000   # оставляем денег
                }

        # options = {
                # 'max_result':3000000,   # поддерживаем максимум золотых лопат
                # 'rezerv_1':63000,       # оставляем резерв бамбука
                # 'rezerv_2':150000000    # оставляем денег
                # }
        
        shovels = self._get_game_state().count_in_storage('@SHOVEL_EXTRA')
        print 'shovels = ', shovels

        # self.craft_count('B_EYE','1',1)
        self.craft(options,'B_EYE','1')

        # actually_crafted, name_result = self.craft_count('B_EYE','1',1)
        # if actually_crafted:
            # logger.info(u'Сделали %d шт. %s'%(actually_crafted, name_result))

        shovels = self._get_game_state().count_in_storage('@SHOVEL_EXTRA')
        print 'shovels = ', shovels
        raw_input('---------------   END   ---------------')
        
        # {u'resultCount': 5L, u'name': u'\u041f\u044f\u0442\u044c \u043b\u043e\u043f\u0430\u0442', u'level': 3L, u'materials': [<game_state.game_event.GameMaterial object at 0x04B411B0>, <game_state.game_event.GameMaterial object at 0x04B41C70>], u'result': u'@SHOVEL_EXTRA', u'delayTime': 0L, u'id': u'1'}
        # {u'count': 10L, u'item': u'@S_12', u'notBeUsed': False}
        # {u'count': 2000L, u'item': u'@COINS', u'notBeUsed': False}
        pass

    def clock(self, timems):
        timems = int(timems)
        h = timems/1000/60/60
        m = timems/1000/60 - (h*60)
        s = round(timems/float(1000) - (timems/1000/60*60), 3)
        ms = timems - (timems/1000*1000)
        # print u'Время: %d:%d:%.3f'%(h,m,s)
        return str(h) + ':' + str(m) + ':' + str(s)

    # cоздание мозгов в останкино
    def ExchangeBrains(self):
        if not self._get_game_state().get_state().playerStatus == u'@PS_HUMAN': return
        options = {
                'max_result':10,    # поддерживаем максимум мозгов без имеющихся у игрока бесплатных
                'rezerv_1':60000,           # оставляем резерв хелий
                'rezerv_2':60000            # оставляем резерв Любви - красных сердец
                }

        brains_maintain = 10 # указываем нужное постоянное количество без имеющихся у игрока бесплатных
        brains_buy = self._get_game_state().get_state().buyedBrains # кол-во активаций мозгов (не самих мозгов) купленных
        brains_const = self._get_game_state().get_state().brainsCount # количество постоянных (от уровня)
        brains_curr = 0 # счетчик кол-ва текущих мозгов
        
        x = 0 # Счетчик кол-ва мозгов с истечением времени < 7 мин.
        print u'current_client_time:', self._get_timer().client_time(), u' -', self.clock(self._get_timer().client_time())
        if len(brains_buy) <> 0:
            for buyed_brain in brains_buy:
                brains_curr += buyed_brain.count
                print u'count', buyed_brain.count, u' endTime', buyed_brain.endTime, u'  - ', self.clock(buyed_brain.endTime), u'осталось:'
                if self._get_timer().has_elapsed_in(buyed_brain.endTime, 7*60):
                    x += buyed_brain.count

        # Разница между необходимыми и текущими мозгами.
        brains_need = brains_maintain - brains_const - brains_curr + x

        print u'Записей brains_buy:', len(brains_buy)
        print u'Мозгов куплено:', brains_curr
        print u'Мозгов постоянных:', brains_const
        print u'Истекает срок меньше чем через 7 мин:', x
        print u'Нужно создать мозгов:', brains_need
        print
        
        # {u'delayTime': 0L, u'name': u'\u041c\u043e\u0437\u0433\u0438', u'level': 3L, u'materials': [{u'count': 2L, u'item': u'@R_12', u'notBeUsed': False}, {u'count':10L, u'item': u'@CR_31', u'notBeUsed': False}], u'result': u'@BRAINS_1', u'playerStatus': u'@PS_HUMAN', u'resultCount': 1L, u'id': u'1'}, 
        
        # {u'delayTime': 0L, u'name': u'7 \u041c\u043e\u0437\u0433\u043e\u0432 \u043d\u0430 \u0434\u0435\u043d\u044c', u'level': 3L, u'materials': [{u'count': 14L, u'item': u'@R_12', u'notBeUsed': False}, {u'count': 70L, u'item': u'@CR_31', u'notBeUsed': False}], u'result': u'@BRAINS_8', u'playerStatus': u'@PS_HUMAN', u'resultCount': 1L, u'id': u'3'},

        raw_input('---------------   END   ---------------')
        
        # Если меньше нужного постоянного, то создаем недостающие.
        result, name_result = self.craft(options,'B_OSTANKINO','1')
        if not result:
            self.craft(options,'B_OSTANKINO_CASH','1')
        
        brains_buy.append(dict2obj({u'count': 1L, u'endTime': u'86400000'})) #Добавляем фейк в список купленных

    # заполнение сундука инструментом
    def fill_pirate_box(self,fill_box):
        for object in self._get_game_state().get_state().gameObjects:
            if object.item == '@PIRATE_BOX' or object.item == '@PIRATE_BOX_2':
                print u'нашли сундук'
                fill_events = []
                if fill_box[instrument] > 0 and self.has_in_storage(instrument,fill_box[instrument]):
                    print 'key', instrument, fill_box[instrument]
                    if fill_box[instrument] > 0:
                        event = {"type":"item","action":"moveInstrumentToBox","count":fill_box[instrument],"itemId":instrument}
                        fill_events.append(event)
                if fill_events:
                    self.send(fill_events)
                    logger.info(u'Наполнили сундук инструментом')
                break

        # building = 'B_PIRATE_CARAVEL_2'
        # build_reader = self._get_item_reader().get(building)
        # has_build = self.check_ship(building,build_reader)
        # print len(has_build)
        # print 'ap_state', self._get_game_state().ap_state
        pass

    def check_ship(self,building,build_reader):
        ap_state = self._get_game_state().ap_state
        ap_state['ship'] = 0
        ap_state['ship_levelup'] = 0
        ap_state['ship_team'] = 0
        has_build = []
        for object in self._get_game_state().get_state().gameObjects:
            if (object.item == '@'+building):
                ap_state['ship'] += 1
                if object.level == len(build_reader.upgrades):
                    ap_state['ship_levelup'] += 1
                    if len(object.team) == build_reader.placesCount:
                        has_build.append(object)
                        ap_state['ship_team'] += 1
        return has_build

    # вывод склада
    def storage2file_full(self):
        storage1 = []
        for object in self._get_game_state().get_state().storageItems:
            name = self._get_item_reader().get(object.item).name
            st = u'' + name.ljust(28, ' ') + object.item.ljust(28, ' ') +unicode(object.count)+'\n'
            storage1.append([name, st])
        storage1.sort()
        st1 = u''
        for t in storage1:
            st1 += t[1]

        storage2 = []
        for object in self._get_game_state().get_state().storageGameObjects:
            name = self._get_item_reader().get(object.item).name
            st = u'' + name.ljust(28, ' ') + object.item.ljust(28, ' ') +unicode(object.count)+'\n'
            storage2.append([name, st])
        storage2.sort()
        st2 = u''
        for t in storage2:
            st2 += t[1]
        
        storage3 = []
        collection = self._get_game_state().get_state().collectionItems
        d = self._get_game_state().get_state().collectionItems.__dict__
        for item in d:
            name = self._get_item_reader().get(item).name
            st = u'' + name.ljust(28, ' ') + item.ljust(28, ' ') +unicode(getattr(collection,item))+'\n'
            storage3.append([item, st])
        storage3.sort()
        st3 = u''
        for t in storage3:
            st3 += t[1]

        text = u'Склад предметов:'+'\n'+'\n' + st1 + '\n'
        text += '\n'+'\n'+u'Склад коллекций:'+'\n'+'\n' + st3 + '\n'
        text += '\n'+'\n'+u'Склад объектов:'+'\n'+'\n' + st2 + '\n'

        with open('storage_' + self.curuser + '.txt', 'w') as f:
           f.write(text.encode('utf-8'))
            
        raw_input('---------------   END   ---------------')
        return
        
    # вывод склада фейков-пиратов
    def storage2file(self):
        predmet = ['@R_33', '@CR_08', '@S_34', '@METAL_SCRAP', '@CR_47', '@CR_53', '@CR_11', '@CR_68', '@CR_33']
        collect = ['C_1', 'C_15']
        
        item = '@CASH'
        name = self._get_item_reader().get(item).name
        zb = u'' + name.ljust(28, ' ') + item.ljust(28, ' ') + unicode(self.cash()) +'\n'
        item = '@COINS'
        name = self._get_item_reader().get(item).name
        coins = u'' + name.ljust(28, ' ') + item.ljust(28, ' ') + unicode(self.money()) +'\n'
        item = '@DUBLON'
        name = self._get_item_reader().get(item).name
        count = self._get_game_state().count_in_storage('@DUBLON')
        dublon = u'' + name.ljust(28, ' ') + item.ljust(28, ' ') + unicode(count) +'\n'

        storage1 = u''
        for mat in predmet:
            name = self._get_item_reader().get(mat).name
            count = self._get_game_state().count_in_storage(mat)
            storage1 += u'' + name.ljust(28, ' ') + mat.ljust(28, ' ') + unicode(count) +'\n'

        storage3 = u''
        for item in collect:
            name = self._get_item_reader().get(item).name
            coll = []
            for num in range(1,6):
                if hasattr(self._get_game_state().get_state().collectionItems, item + '_' + str(num)):
                    coll.append(getattr(self._get_game_state().get_state().collectionItems, item + '_' + str(num)))
            if len(coll) < 5:
                count = 0
            else:
                count = min(coll)
            storage3 += u'' + name.ljust(28, ' ') + item.ljust(28, ' ') + unicode(count) +'\n'

        text = zb + coins + dublon
        text += '\n'+'\n' + storage1
        text += '\n'+'\n' + storage3
        # text += '\n'+'\n' + storage2 + '\n'

        with open('storage_' + self.curuser + '.txt', 'w') as f:
           f.write(text.encode('utf-8'))


    """
    # Сортируем список друзей по уровню
    if True:
        fr_dict = {info.id : info.level for info in players_info}
        friends_order = fr_dict.items()
        friends_order.sort(key=lambda x: x[:-1], reverse=True)
        friends = [fr[0] for fr in friends_order]
    
        # Пример
        fr_dict = {'567':5, '123':1, '345':3, '456':4}
        friends_order = fr_dict.items()
        friends_order.sort(key=lambda x: x[:-1], reverse=True)
        friends = [fr[0] for fr in friends_order]
    """
    
    # срок действия
    """
    if True:
        1448260102.17  -сейчас
        {u'item': u'@TURKEY_BOX', u'startDate': u'1448485200000', u'endDate': u'1448571540000', u'mission': u'Q_TH15_7', u'level': 0L}, 
        Старт  25 Nov 2015 21:00:00 GMT
        Конец  26 Nov 2015 20:59:00 GMT
    """
