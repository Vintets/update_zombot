# coding=utf-8
import logging
from game_state.game_types import GameWoodGrave, GameWoodGraveDouble,\
    GamePickItem, GameWoodTree, GameStone, GameGainItem, GamePickup
from game_state.game_event import dict2obj
from game_state.base import BaseActor
logger = logging.getLogger(__name__)


class Barabashka(BaseActor):
    def perform_action(self):
        if self.if_location_pirate(): return
        self.curuser = str(self._get_game_state().get_curuser())
        path_akkstat = self.mega().get_path_akkstat()
        self.path_akkstat_curuser = path_akkstat + self.curuser + '\\'
        towers = self._get_game_location().get_all_objects_by_type('halloweenTower')
        ghost = 0
        for tower in towers:
            for i in tower.users:
                reader_pack = self._get_item_reader().get(i.itemId)
                ghost += int(reader_pack.count)
                apply_ghost = {"objId":tower.id,"itemId":i.itemId,"action":"trick","extraId":i.id,"type":"item"}
                self._get_events_sender().send_game_events([apply_ghost])
                self._get_game_state().add_from_storage(reader_pack.item,reader_pack.count)
            tower.users = []
        if ghost > 0: print u" Собрал %d барабашек" % ghost 
        return

        # бантики
        bows = self._get_game_location().get_all_objects_by_type('halloweenTower')

        if not hasattr(self._get_game_state(), 'circus_user'):
            try:
                with open(self.path_akkstat_curuser + 'circus_user.txt', 'r') as f:
                    self._get_game_state().circus_user = eval(f.read())
            except:
                self._get_game_state().circus_user = []

        self._event = []
        self.bow_count = 0
        for bow in bows:
            if bow.item == '@B_TENT_CIRCUS':
                for i in bow.users:
                    self._get_game_state().circus_user.append(i.id)
                    self._event.append({"extraId":i.id,"itemId":i.itemId,"action":"trick","type":"item","objId":bow.id})
                    name_user = self.addName(i.id)
                    sp_n = u' '*(14-len(name_user))
                    name = unicode(i.id).ljust(20, " ") + name_user + sp_n
                    if i.itemId == u'BOW_PACK_DEFAULT':
                        count = 1
                    elif i.itemId == u'BOW_PACK_SMALL':
                        count = 3
                    elif i.itemId == u'BOW_PACK_MEDIUM':
                        count = 10
                    sms = u'Пользователь ' + name + u' положил бантиков: ' + unicode(count)
                    logger.info(u'Пользователь %s положил бантиков: %d' % (name, count))
                    with open(self.path_akkstat_curuser + 'action_frends.txt', 'a') as f:
                        sms += u'\n'
                        f.write(sms.encode("utf-8"))
                    self.bow_count += count
                    if len(self._event) > 499:
                        self.events_send()
                bow.users = []
                self.events_send()

    def events_send(self):
        if self._event != []:
            self._get_events_sender().send_game_events(self._event)
            # добавляем на склад
            self._get_game_state().add_from_storage("@CR_153",self.bow_count)
            with open(self.path_akkstat_curuser + 'circus_user.txt', 'w') as f:
                f.write(str(self._get_game_state().circus_user))
            print
            if str(len(self._event))[-1:] == '1' and len(self._event) !=11:
                if str(self.bow_count)[-1:] == '1' and self.bow_count !=11:
                    logger.info(u'Собрали %d бантик   от %d друга' % (self.bow_count, len(self._event)))
                elif 1 < int(str(self.bow_count)[-1:]) < 5 and self.bow_count < 5 and self.bow_count > 20:
                    logger.info(u'Собрали %d бантика  от %d друга' % (self.bow_count, len(self._event)))
                else:
                    logger.info(u'Собрали %d бантиков от %d друга' % (self.bow_count, len(self._event)))
            else:
                if str(self.bow_count)[-1:] == '1' and self.bow_count !=11:
                    logger.info(u'Собрали %d бантик   от %d друзей' % (self.bow_count, len(self._event)))
                elif 1 < int(str(self.bow_count)[-1:]) < 5 and self.bow_count < 5 and self.bow_count > 20:
                    logger.info(u'Собрали %d бантика  от %d друзей' % (self.bow_count, len(self._event)))
                else:
                    logger.info(u'Собрали %d бантиков от %d друзей' % (self.bow_count, len(self._event)))
            self._event = []
            self.bow_count = 0

    def addName(self, id):
        if hasattr(self._get_game_state(), 'friends_names') and self._get_game_state().friends_names.get(id) and self._get_game_state().friends_names.get(id) != u'Без имени':
            name = u" '" + self._get_game_state().friends_names.get(id) + u"'"
            name = name.replace(u'\u0456', u'i').encode("UTF-8", "ignore")
            name = unicode(name, "UTF-8")
            #print name.replace(u'\u0456', u'i').encode("cp866", "ignore")
        else: name = u''
        return name

"""
print (u'Собрали %(+4)d бантиков у %(+4)d друзей') % (20, 7)
u'remoteTrickTreating': [
{u'count': 0L, u'date': u'-14202657', u'user': u'17823741170892930057', u'item': u'@CR_153'}, 
{u'count': 0L, u'date': u'-14087056', u'user': u'11628291596603049922', u'item': u'@CR_153'},


{"type":"bellBox","id":"BOW_PACK_DEFAULT","name":"Бабочка","level":1,"buyCoins":0,"buyCash":0,"openCash":0,"gift":false,"xp":1,"shopImage":"crops/m_bow_tie.png","count":1,"item":"@CR_153","expire":43200,"disabled":false},
{"type":"bellBox","id":"BOW_PACK_SMALL","name":"3 Бабочки","level":1,"buyCoins":0,"buyCash":1,"openCash":0,"gift":true,"xp":1,"shopImage":"crops/m_bow_tie_3.png","count":3,"item":"@CR_153","disabled":false},
{"type":"bellBox","id":"BOW_PACK_MEDIUM","name":"10 Бабочек","level":1,"buyCoins":0,"buyCash":4,"openCash":0,"gift":true,"xp":1,"shopImage":"crops/m_bow_tie_5.png","count":10,"item":"@CR_153","disabled":false}

принять
{"extraId":"864730088123287177","itemId":"BOW_PACK_DEFAULT","action":"trick","type":"item","objId":24578}

FIND:  @B_TENT_CIRCUS   id =  24578
u'level': 0L, u'item': u'@B_TENT_CIRCUS', u'y': 70L, u'x': 14L, u'type': u'halloweenTower', u'id': 24578L, u'rotate': 0L, u'users':
{u'itemId': u'BOW_PACK_DEFAULT', u'expire': u'38559575', u'id': u'6064366433377019163'}, 
{u'itemId': u'BOW_PACK_DEFAULT', u'expire':u'38637626', u'id': u'7121282654775318802'}, 
{u'itemId': u'BOW_PACK_DEFAULT', u'expire': u'38832674', u'id': u'15818464092022489540'}, 
{u'itemId': u'BOW_PACK_DEFAULT', u'expire': u'38901416', u'id': u'14055452232076276468'}, 
{u'itemId': u'BOW_PACK_DEFAULT', u'expire': u'38977137', u'id': u'15014405591850557706'}, 
{u'itemId': u'BOW_PACK_DEFAULT', u'expire': u'39026125', u'id': u'4291172554993809160'}, 
{u'itemId': u'BOW_PACK_SMALL', u'id': u'4291172554993809160'}, 
{u'itemId': u'BOW_PACK_SMALL', u'id': u'4291172554993809160'}, 
{u'itemId': u'BOW_PACK_SMALL', u'id': u'4291172554993809160'}, 
{u'itemId': u'BOW_PACK_SMALL', u'id': u'4291172554993809160'}, 
""" 
