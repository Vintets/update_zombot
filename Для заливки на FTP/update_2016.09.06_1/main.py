#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
# os.chdir('C:\\Python27')
import logging
import errno
import sys
import re
sys.path.append('./API')
import game_state.colorprint as cp
from game_state.user_interface import UserPrompt
from game_state.game_engine import Game
from game_state.connection import Connection
from game_state.settings import Settings
from game_state.sn import Site
from _mega_options import MegaOptions
from ctypes import windll

stdout_handle = windll.kernel32.GetStdHandle(-11)
SetConsoleTextAttribute = windll.kernel32.SetConsoleTextAttribute

logger = logging.getLogger('main')
os.system('color 71')

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError, e:
        if e.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

def setup_basic_logging(gui_logger):
    #FORMAT = '[%(asctime)s] %(message)s'
    FORMAT = '%(asctime)s.%(msecs)d %(message)s'
    logging.basicConfig(level=logging.INFO, format=FORMAT, datefmt='%H:%M:%S', # , filename='my_app.log'
                        stream=MyLogger(gui_logger))
    connection_logger = logging.getLogger('connection')
    connection_logger.propagate = False

def setup_file_logging(path_akkstat_curuser, log_level):
    log_directory = path_akkstat_curuser + 'logs\\'
    mkdir_p(log_directory)
    connection_logger = logging.getLogger('connection')
    connection_logger.propagate = False
    connection_logger.addHandler(
            logging.FileHandler(log_directory + 'connection.log')
            )
    connection_logger.setLevel(log_level)
    unknownEventLogger = logging.getLogger('unknownEventLogger')
    unknownEventLogger.propagate = False
    unknownEventLogger.addHandler(
            logging.FileHandler(log_directory + 'unknown_events.log')
            )
    unknownEventLogger.setLevel(log_level)

def strip_special(string):
    return string
    return ''.join(e for e in string if e.isalnum())

def set_folder(selected_user):
    _mega = MegaOptions(selected_user)
    path_akkstat = _mega.get_path_akkstat()
    path_akkstat_curuser = path_akkstat + selected_user + '\\'
    if not os.path.isdir('loaded_res'): os.makedirs('loaded_res')
    if not os.path.isdir('statistics'): os.makedirs('statistics')
    if not os.path.isdir(path_akkstat): os.makedirs(path_akkstat)
    if not os.path.isdir(path_akkstat + selected_user):
        os.makedirs(path_akkstat + selected_user)
    return path_akkstat_curuser

def get_site(gui_input):
    settings = Settings()
    default_user = settings.get_default_user()
    users = settings.getUsers()
    #selected_user = UserPrompt(gui_input).prompt_user('Select user:', users)
    if len(sys.argv) > 1 and sys.argv[1].isdigit():
        #logger.info('пользователь передан с параметром 1')
        selected_user = users[int(sys.argv[1])]
    elif len(sys.argv) > 2 and sys.argv[2].isdigit():
        #logger.info('пользователь передан с параметром 2')
        selected_user = users[int(sys.argv[2])]
    elif default_user == None:
        selected_user = UserPrompt(gui_input).prompt_user('Select user:', users)
    else:
        selected_user = users[int(default_user)]

    print 'You selected "' + selected_user + '"'

    path_akkstat_curuser = set_folder(selected_user)
    log_level = settings.get_file_log_level()
    setup_file_logging(path_akkstat_curuser, log_level)
    settings.setUser(selected_user)
    site = Site(settings)        
    return site, settings

def run_game(gui_input=None):
    setup_basic_logging(gui_input)
    logger.info('Выбираем пользователя...')
    site, settings = get_site(gui_input)
    Game(site, settings, UserPrompt(gui_input), gui_input=gui_input).start()

MyLogger = None

BRANCH = 'master by Vanuan'
__version__ = '0.9.8++ ' + BRANCH + ' changed by Vint' + ' and many, many other...'
__copyright__ = '2013-2016 (c) github.com/Vanuan/zombot'

if __name__ == '__main__':
    #print '\n2013 (c) github.com/Vanuan/zombot\n version %s\n\n' % __version__
    #print '#'*80+'2013 (c) github.com/Vanuan/zombot\n version %s\n' % (__version__)+'#'*80

    print (u' '*80*3)
    # cp.cprint(u'1%s' % (u' '*79*20))
    line1 = ' '*((80-len(__copyright__))/2)
    line2 = ' '*((80-len(__version__)-8)/2)
    print '#'*80+line1+__copyright__+line1+"\n"+line2+"version %s"%(__version__)+line2+"\n"+'#'*80
    name_product = "!!! ZomBot !!!"
    tab = ' '*((80-len(name_product))/2)
    print tab + name_product + tab
    SetConsoleTextAttribute(stdout_handle, 0x0001 | 0x0070)

    if len(sys.argv) == 1 or (len(sys.argv) == 2 and sys.argv[1] != '-c'):
        import game_state.gui
        MyLogger = game_state.gui.MyLogger
        import game_state.app
        app.run_application(run_game)
    elif len(sys.argv) > 2 and (sys.argv[1] == '-c' or sys.argv[2] == '-c'):
        import game_state.console
        MyLogger = game_state.console.MyLogger
        run_game()
    else:
        import game_state.console
        MyLogger = game_state.console.MyLogger
        run_game()
