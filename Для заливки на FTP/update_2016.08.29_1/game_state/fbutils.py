#!/usr/bin/python
# coding=utf-8
#(c) 2016 greyzza

import re
import json
import requests
from connection import Connection
from game_state.game_types import GameSTART, GameInfo
from _mega_options import MegaOptions


class FB():
    def __init__(self, credentials):
        self._credentials = credentials
        self.base_url = 'https://www.facebook.com/'
        self.app_id = 'zombieisland'
        self.game_url = 'https://zombie-fb.shadowlands.ru/zombiefb/'
        self.game_client_id = '253266951357003'
        self.curuser = None

    def get_game_params(self):
        params = self.getAppParams()
        self.curuser = self.__game_api_user_id = params['user']
        game_auth_key = params['auth']
        connection = Connection(self.game_url + 'go')
        self.__params = params
        return (self.__game_api_user_id, game_auth_key, params, connection)

    def getAppParams(self, session_cookies=None):
        if session_cookies is None: session_cookies = self._getSessionCookies()
        session_cookies = self.str2dict(session_cookies)
        redirect_uri = 'http://apps.facebook.com/' + self.app_id + '/'
        res = requests.get(redirect_uri, cookies=session_cookies)
        matcher = re.compile(r'<input type="hidden" autocomplete="off" name="([^"]+)" value="([^"]*)" />')
        post = dict(matcher.findall(res.content))
        params = {}
        params['key'] = post['signed_request']
        mr = Connection(self.game_url)
        html = mr.sendRequest(post, None)
        matcher = re.compile('.*flashvars = (.*);$')
        params_list = None
        for line in html.split('\n'):
            match = matcher.match(line)
            if match is None: continue
            params_list = match.group(1)
            break
        if len(params_list) <= 0 or params_list is None or params_list == '{}': return False
        params_list = params_list.replace('{','').replace('}','').replace("'",'').replace(" ",'').split(',')
        params.update(dict((param.split(':')[0], param.split(':')[1]) for param in params_list))
        self.__session_cookies = session_cookies
        return params

    def get_time_key(self):
        return self.__params['key']

    def create_start_command(self,server_time, client_time):
        command = GameSTART(lang=u'en', info=self._getUserInfo(),
                            ad=u'search', serverTime=server_time, clientTime=client_time)
        # self.__mega = MegaOptions(self.curuser)
        friends = self._getFriendsListFB()
        return command, friends

    def _getUserInfo(self):
        return GameInfo()

    def str2dict(self, val):
        if not type(val) is str: return val
        res = dict((tmp.split('=')[0], tmp.split('=')[1]) for tmp in val.replace(' ','').split(';') if tmp)
        return res

    def _getFriendsListFB(self):
        session_cookies = self.__session_cookies
        access_token = self._get_access_token(session_cookies)
        if not access_token: return[]
        print u'Загружаем список друзей...'
        fields = 'id'
        post_wishes = {'access_token': access_token, 'fields':fields}
        newPath = 'https://graph.facebook.com/v2.1/me/friends'
        html = requests.get(newPath, cookies=session_cookies, params=post_wishes)
        result = json.loads(html.text)
        friendsid = [user['id'] for user in result['data']]
        print (u'Всего друзей в списке: %s' % str(len(friendsid))).encode('cp866')
        return friendsid

    def _get_access_token(self,session_cookies):
        redirect_uri = 'http://apps.facebook.com/' + self.app_id + '/'
        parGet = {'client_id':self.game_client_id,
                'scope':'public_profile,email,user_friends',
                'redirect_uri':redirect_uri,
                'response_type':'token'
                }
        tokenUrl = self.base_url + 'v2.1/dialog/oauth'
        token_code = requests.get(tokenUrl, params=parGet, cookies=session_cookies, allow_redirects=False)
        try: return token_code.headers['location'].split('&')[0].split('=')[1]
        except: return False

    def _validateSessionCookies(self, session_cookies):
        if session_cookies is None: return False
        if not self.getAppParams(session_cookies): return False
        return True

    def _getSessionCookies(self):
        session_cookies = self._credentials.getSessionCookies()
        cookies_are_valid = self._validateSessionCookies(session_cookies)
        if cookies_are_valid: return session_cookies
        print u' Session old.Autentification...',
        username = self._credentials.getUserEmail()
        password = self._credentials.getUserPassword()
        s = requests.Session()
        login_url = self.base_url + 'login.php'
        params = {'login_attempt':1}
        s.get(self.base_url)
        post = {'email':username, 'pass':password}
        s.post(login_url, data=post, params=params)
        session_cookies = ''
        for key in s.cookies.keys():
            session_cookies += str(key) + '=' + str(s.cookies[key]) + ';'
        s.close()
        self._credentials.setSessionCookies(session_cookies)
        return session_cookies
