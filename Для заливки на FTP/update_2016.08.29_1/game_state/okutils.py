#!/usr/bin/python
# coding=utf-8
#(c) 2016 greyzza
import re
import json
import requests
from hashlib import md5
from connection import Connection
from game_state.game_types import GameSTART, GameInfo


GAME_URL='http://jok.shadowlands.ru/zombieok/go'

class OK():
    def __init__(self, credentials):
        self._credentials = credentials

    def get_time_key(self):
        del self.__params['sig']
        return self.__params['session_key']

    def getFriends(self):
        return self.friendsid

    def getMyId(self):
        return self.__params['logged_user_id']

    def str2dict(self, val):
        if not type(val) is str:return val
        res=dict((tmp.split('=')[0],tmp.split('=')[1]) for tmp in val.replace(' ','').split(';') if tmp)
        return res

    def create_start_command(self, server_time, client_time):
        command = GameSTART(lang=u'en', info=self._getUserInfo(),ad=u'search',
                            serverTime=server_time,clientTime=client_time)
        self.friendsid = self._getFriendsListOK()
        return command, self.friendsid

    def getAppParams(self, session_cookies=None):
        if session_cookies is None:session_cookies = self._getSessionCookies()
        session_cookies = self.str2dict(session_cookies)
        self.__session_cookies=session_cookies
        html = requests.get('http://ok.ru', cookies=session_cookies).text
        requested = '?gwt.requested='
        matcher = re.compile('.*gwtHash:"(.*?)"')
        if not html:return False
        for line in html.split('\n'):
            match = matcher.match(line)
            if match is None:continue
            requested += match.group(1)
            break
        request_url = 'http://ok.ru/game/zm' + requested
        html = requests.get(request_url, cookies=session_cookies).text
        params = None
        if not html:return False
        matcher = re.compile('.*zombiefarm.html\?(.*?)"')
        for line in html.split('\n'):
            match = matcher.match(line)
            if match is not None:
                params = match.group(1)
                break
        if params is None:return False
        orig_params = params
        pairs = params.split('&amp;')
        params = dict((pair.split('=')[0],pair.split('=')[1]) for pair in pairs)
        return params

    def get_game_params(self):
        params = self.getAppParams()
        ok_user_id = params['logged_user_id']
        ok_auth_key = params['auth_sig']
        ok_session_key = params['session_secret_key']
        connection = Connection(GAME_URL)
        self.__params = params
        # self._ok_user_id = ok_user_id
        return (ok_user_id, ok_auth_key, ok_session_key, connection)

    def _getUserInfo(self):
        getInfo_url = 'http://api.ok.ru/api/users/getInfo'
        post = {
                'uids': self.__params['logged_user_id'],
                'new_sig': 1,
                'session_key': self.__params['session_key'],
                'fields': u'uid,first_name,last_name,gender,birthday,locale,location',
                'application_key': self.__params['application_key'],
                'format': 'Json'
                }
        post_keys = sorted(post.keys())
        param_str = ''.join(['%s=%s' % (str(key), self._encode(post[key])) for key in post_keys])
        param_str += self.__params['session_secret_key']
        sign = md5(param_str).hexdigest().lower()
        post.update({'sig': sign})
        info = requests.post(getInfo_url, data=post,
                cookies=self.str2dict(self._credentials.getSessionCookies())).json()[0]
        game_info = GameInfo(city=info['location']['city'],
                            first_name=info['first_name'],
                            last_name=info['last_name'],
                            uid=long(info['uid']),
                            country=info['location']['country'],
                            bdate=info['birthday']
                            )
        global uid
        uid = info['uid']
        return game_info

    def _getFriendsListOK(self):
        getAppUsers_url='http://api.ok.ru/api/friends/getAppUsers'
        post = {
                'new_sig': 1,
                'session_key': self.__params['session_key'],
                'application_key': self.__params['application_key'],
                'format': 'Json'
                }
        post_keys = sorted(post.keys())
        param_str = ''.join(['%s=%s' % (str(key), self._encode(post[key])) for key in post_keys])
        param_str += self.__params['session_secret_key']
        sign = md5(param_str).hexdigest().lower()
        post.update({'sig': sign})
        info = requests.post(getAppUsers_url, data=post, cookies=self.__session_cookies).json()['uids']
        return info

    def _validateSessionCookies(self, session_cookies):
        if session_cookies is None:return False
        if not self.getAppParams(session_cookies):return False
        return True

    def _encode(self,s):
        if isinstance(s, (dict, list, tuple)):
            s = json.dumps(s, ensure_ascii=False, encoding='utf8')
        if isinstance(s, unicode):s = s.encode('utf8')
        return s

    def _getSessionCookies(self):
        session_cookies = self._credentials.getSessionCookies()
        cookies_are_valid = self._validateSessionCookies(session_cookies)
        if not cookies_are_valid:
            print u' Session old.Autentification...',
            username = self._credentials.getUserEmail()
            password = self._credentials.getUserPassword()
            s = requests.Session()
            base_url = 'https://www.ok.ru/'
            login_url = 'https://www.ok.ru/https'
            post = {'st.email': username,'st.password': password}
            res = requests.get(base_url)
            matcher = re.compile(r'<input type="hidden" name="([^"]*)" value="([^"]*)" ')
            post.update(dict(matcher.findall(res.content)))
            post.update({'st.posted':'set','st.remember': 'on','button_go': 'Sign in'})
            sslurl = requests.post(login_url, data=post, allow_redirects=False, verify=True).headers['location']
            cookies = requests.get(sslurl, allow_redirects=False).cookies
            session_cookies = ''
            for key in cookies.keys():
                session_cookies += str(key) + '=' + str(cookies[key]) + ';'
            self._credentials.setSessionCookies(session_cookies)
        return session_cookies

    def _get_friend_info(self, friend):
        post = {
                'uids': friend,
                'new_sig': 1,
                'session_key': self.__params['session_key'],
                'fields': u'uid, first_name, last_name, gender, birthday, locale, location',
                'application_key': self.__params['application_key'],
                'format': 'Json'
                }
        getInfo_url = 'http://api.ok.ru/api/users/getInfo'
        post_keys = sorted(post.keys())
        param_str = ''.join(['%s=%s' % (str(key), self._encode(post[key])) for key in post_keys])
        param_str += self.__params['session_secret_key']
        sign = md5(param_str).hexdigest().lower()
        post.update({'sig': sign})
        info = requests.post(getInfo_url, data=post, cookies=self.str2dict(self._credentials.getSessionCookies())).json()[0]
        # print(info)
