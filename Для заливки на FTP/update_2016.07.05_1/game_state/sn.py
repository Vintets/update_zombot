# -*- coding: utf-8 -*-

import time
import game_state.vkutils
import game_state.mrutils
import game_state.okutils
import game_state.fbutils

def Site(settings):
    if settings.getSite() == 'mr':
        return game_state.mrutils.MR(settings)
    elif settings.getSite() == 'ok':
        return game_state.okutils.OK(settings)
    elif settings.getSite() == 'vk':
        return game_state.vkutils.VK(settings)
    elif settings.getSite() == 'fb':
        return game_state.fbutils.FB(settings)
    else:
        print u'Неправильно задана социальная сеть'
        time.sleep(10)
        exit(1)
