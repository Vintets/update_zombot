#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import re
import time
import shutil
from ctypes import windll

stdout_handle = windll.kernel32.GetStdHandle(-11)
SetConsoleTextAttribute = windll.kernel32.SetConsoleTextAttribute

class ChangeMega():
    def __init__(self, test):
        self.test = test
        self.filename = u'_mega_options.py'
        self.all_text = u''
        self.data = {}
        # current = sys.argv[1]
        # print 'current', current
        # print os.getcwd()
        # print
        # print u'обновлялка меги'
        pass

    def run(self):
        if os.path.isfile('API\\vkontakte\\api.py'):
            os.remove('API\\vkontakte\\api.py')
        try:
            shutil.move('api.py', 'API\\vkontakte\\')
        except:
            print 'Error MOVE file api.py'
            self.exit_error(u'Error MOVE file api.py')
        self.exit_ok()

    def exit_ok(self):
        if self.test:
            raw_input('---------------   END   ---------------')
        sys.exit(0)

    def exit_error(self, text):
        self.cprint(u'12%s' % text)
        time.sleep(3)
        if self.test:
            raw_input('---------------   END   ---------------')
        sys.exit(1)

    def cprint(self, cstr):
        clst = cstr.split('^')
        color = 0x0001
        for cstr in clst:
            dglen = re.search("\D", cstr).start()
            color = int(cstr[:dglen])
            text = cstr[dglen:]
            if text[:1] == "_": text = text[1:]
            SetConsoleTextAttribute(stdout_handle, color | 0x0070) #78
            print text.replace(u'\u0456', u'i').encode("cp866", "ignore"),
        #sys.stdout.flush()
        print ""
        SetConsoleTextAttribute(stdout_handle, 0x0001 | 0x0070)


if __name__ == '__main__':
    test = False
    change_mega = ChangeMega(test)
    change_mega.run()

