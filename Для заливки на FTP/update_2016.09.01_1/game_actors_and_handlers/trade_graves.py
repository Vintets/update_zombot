# coding=utf-8
import logging
from game_state.base import BaseActor
from game_state.game_event import obj2dict, obj2dict

logger = logging.getLogger(__name__)


class TradeBot(BaseActor):
    safe_collection = ['@C_42','@C_29']

    def perform_action(self):
        if self.if_location_pirate(): return
        traders = self.get_traders()
        if not traders: return

        for _obj in traders:
            if _obj.countExchange == 0 and _obj.countCompleted == 0:
                give_list = obj2dict(_obj.lastGiven)
                # print '_obj.give', give_list
                give_t = {giv['item']:giv['count'] for giv in give_list}

                if not self.check_give(give_t): continue
                if hasattr(_obj, 'user'):
                    user = _obj.user
                else:
                    user = ''
                self.event_exchange(_obj.lastWanted, give_list, _obj, user, ext_text=u'повторный ')

    def get_traders(self, id=None, set=False):
        traders = []
        for _obj in self._get_game_location().get_game_objects():
            if 'SC_TRADER_GRAVE' not in _obj.item: continue
            if (not set) and _obj.countExchange == 1:
                self.start_trader(_obj)
                continue
            if _obj.countCompleted == 1:
                self.pick_exchange(_obj)
            if id and (_obj.id not in id):
                self.start_trader(_obj)
                continue
            traders.append(_obj)
        return traders

    def pick_exchange(self, _obj):
        if _obj.countCompleted == 1:
            trader_event = {'objId':_obj.id,
                            'type':'item',
                            'action':'pick'}
            self._get_events_sender().send_game_events([trader_event])
            print u'Забираем коробку у торгаша № ',_obj.id
            _obj.countCompleted = 0

    def event_exchange(self, want, give, _obj, user='', ext_text=u''):
        event = {
                'want':want,
                'give':give,
                'type':'trader',
                'countExchange':1,
                'action':'change',
                'objId':_obj.id
                }
        if user != '': event['user'] = user
        self._get_events_sender().send_game_events([event])
        print u'Ставим %sторг у торгаша № %d' % (ext_text, _obj.id)
        _obj.countExchange = 1
        self.remove_tovar(give)
        self.start_trader(_obj)

    def start_trader(self, _obj):
        if _obj.started == False:
            trader_event = {'objId':_obj.id,
                            'type':'item',
                            'action':'start'}
            self._get_events_sender().send_game_events([trader_event])
            print u'Выгоняем на работу торгаша № ',_obj.id
            _obj.started = True

    def remove_tovar(self, give):
        for slot_dict in give:
            item = slot_dict['item']
            count = slot_dict['count']
            if not item.startswith('@'):
                item = '@' + item
            if item.startswith('@C_') and len(item) > 5:
                item_lstrip = item.lstrip('@')
                collectionItems = self._get_game_state().get_state().collectionItems
                has = 0 if not hasattr(collectionItems, item_lstrip) else getattr(collectionItems, item_lstrip)
                setattr(collectionItems, item_lstrip, has - count)
            elif item.startswith('@C_'):
                item_lstrip = item.lstrip('@')
                collectionItems = self._get_game_state().get_state().collectionItems
                for num in range(1, 6):
                    item_ = item_lstrip + '_' + str(num)
                    has = 0 if not hasattr(collectionItems, item_) else getattr(collectionItems, item_)
                    setattr(collectionItems, item_, has - count)
            else:
                self._get_game_state().remove_from_storage(item, count)

    def check_give(self, give_t):
        check = True
        for item in give_t.keys():
            count_required = give_t[item]
            if not item.startswith('@'):
                item = '@' + item
            if item[:5] in TradeBot.safe_collection: continue
            if item.startswith('@C_') and len(item) > 5:
                item_lstrip = item.lstrip('@')
                collectionItems = self._get_game_state().get_state().collectionItems
                has = 0 if not hasattr(collectionItems, item_lstrip) else getattr(collectionItems, item_lstrip)
            elif item.startswith('@C_'):
                item_lstrip = item.lstrip('@')
                collectionItems = self._get_game_state().get_state().collectionItems
                coll = []
                for num in range(1, 6):
                    if hasattr(collectionItems, item_lstrip + '_' + str(num)):
                        coll.append(getattr(collectionItems, item_lstrip + '_' + str(num)))
                if len(coll) < 5:
                    name = self._get_item_reader().get(item).name
                    check = False
                    break
                has = min(coll)
            else:
                has = self._get_game_state().count_in_storage(item)
            if has < count_required:
                name = self._get_item_reader().get(item).name
                check = False
                break
        if not check:
            self.cprint(u"4Для выставления обмена торговцу не хватает '" + name + "'")
        return check


class TradeSet(TradeBot):
    def opt(self):
        want = self.mega().trade_options().get('want')
        give = self.mega().trade_options().get('give')
        id = self.mega().trade_options().get('id', None)
        user = self.mega().trade_options().get('user', '')
        set = self.mega().trade_options().get('set', False)
        if type(id) is int: id = [id]
        elif id is None: id = []
        if user == 'null': user = ''
        return (want, give, id, user, set)

    def perform_action(self):
        if self.if_location_pirate(): return
        want_t, give_t, id, user, set = self.opt()
        if not (want_t and give_t): return
        traders = self.get_traders(id=id, set=set)
        if not traders: return
        _obj = traders[0]

        want = [{'count':w, 'item':val} for w, val in want_t.items()]

        if not self.check_give(give_t): return
        give = []
        for w in give_t.keys():
            if not w.startswith('@'):
                w = '@' + w
            give.append({'count':give_t[w],'item':w})

        self.event_exchange(want, give, _obj, user, ext_text=u'спец.')


class TradeAutoColl(TradeSet):
    safe_collection = ['@C_42','@C_29']

    def opt(self):
        want = self.mega().trade_auto_coll().get('want')
        give = self.mega().trade_auto_coll().get('give_coll')
        id = self.mega().trade_auto_coll().get('id', None)
        user = self.mega().trade_auto_coll().get('user', '')
        full = self.mega().trade_auto_coll().get('full', False)
        all = self.mega().trade_auto_coll().get('all', False)
        if type(id) is int:id = [id]
        elif id is None: id = []
        if user == 'null': user = ''
        return (want, give, id, user, full, all)
        
    def perform_action(self):        
        if self.if_location_pirate(): return
        want_t, give_t, id, user, full, all = self.opt()
        if not (want_t and give_t): return
        traders = self.get_traders(id=id)
        if not traders: return

        want = [{'item':w, 'count':val} for w, val in want_t.items()]
        has_full, has_not_full = self.get_has_coll(give_t, full, all)
        lock_f = []
        lock_nf = []
        for _obj in traders:
            give = []
            for g in range(4):
                selected_item = None
                selected_count = None
                for ind, elem in enumerate(has_full):
                    item = elem[0]
                    count = elem[1]
                    if item in lock_f: continue
                    if count > 1000:
                        count = 1000
                        has_full[ind][1] -= 1000
                    else:
                        temp = has_full.pop(ind)
                    selected_item = item
                    selected_count = count
                    lock_f.append(item)
                    break
                else:
                    for ind, elem in enumerate(has_not_full):
                        item = elem[0]
                        count = elem[1]
                        if (item in lock_nf) or (item[:-2] in lock_f): continue
                        selected_item = item
                        selected_count = count
                        lock_nf.append(item)
                        temp = has_not_full.pop(ind)
                        break
                if selected_item:
                    give.append({'count':selected_count,'item':selected_item})
                else: break
            print give
            if give:
                self.event_exchange(want, give, _obj, user, ext_text=u'коллекционный ')

    def get_has_coll(self, give_t, full, all):
        has_full = []
        has_not_full = []
        all_check = []
        if all: # если пробежаться по всем
            all_check = ['@C_' + str(i) for i in range(1, 61) 
                        if '@C_' + str(i) not in TradeAutoColl.safe_collection]
        else:   # если только заданные
            for item in give_t.keys():
                if not item.startswith('@'): item = '@' + item
                if (not item.startswith('@C_')) or len(item) > 5: continue
                if item[:5] in TradeAutoColl.safe_collection: continue
                all_check.append(item)

        for coll in all_check:
            count_f, count_nf = self.get_free_full_count(coll, give_t.get(coll, 0), full)
            if count_f: has_full.append(count_f)
            if count_nf and not full: has_not_full.extend(count_nf)
        # print 'has_full \n', has_full
        # print 'has_not_full \n', has_not_full
        return has_full, has_not_full

    def get_free_full_count(self, coll, reserv, full):
        count_f = None
        count_nf = []
        collectionItems = self._get_game_state().get_state().collectionItems
        coll_lstrip = coll.lstrip('@')
        five_elem = []
        for num in range(1, 6):
            if hasattr(collectionItems, coll_lstrip + '_' + str(num)):
                five_elem.append(getattr(collectionItems, coll_lstrip + '_' + str(num)))
        if len(five_elem) == 5:
            excess = min(five_elem) - reserv
            if excess > 0: count_f = [coll, excess]
        if not full:
            if excess < 0: excess = 0
            for num in range(1, 6):
                item = coll + '_' + str(num)
                item_lstrip = coll_lstrip + '_' + str(num)
                if hasattr(collectionItems, item_lstrip):
                    has = getattr(collectionItems, item_lstrip)
                    excess = has - reserv
                    if count_f: excess -= count_f[1]
                    if excess > 0: count_nf.append([item, excess])
        return count_f, count_nf
