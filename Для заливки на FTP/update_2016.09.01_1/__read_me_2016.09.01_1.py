
обновление 2016.09.01_1

statistics\report_form.py

game_actors_and_handlers\trade_graves.py
Добавлен актёр TradeAutoColl - выставление коллекций в обмен автоматом, с учётом лимита.



в _mega_options:

в импорт добавляется 'TradeAutoColl'
    from game_actors_and_handlers.trade_graves import TradeBot, TradeSet, TradeAutoColl


в выборе модулей
после
                    # TradeSet,              # Супер торговец (админы добавили автобан! Аккуратно!)
добавляется параметр
                    # TradeAutoColl,         # Ставить авто обмен коллекций у торговцев


в опции # Разное =======================================================================
после настроек
    def trade_options(self):
добавляются настройки
    def trade_auto_coll(self):


будет выглядеть примерно так:
    # ставим авто обмен коллекций у торговцев TradeAutoColl (-trade_graves.py-)
    def trade_auto_coll(self):
        if self.group == 1:
            return {
                    'give_coll':{                       # отдаём : оставлять!
                                '@C_31':1000,
                                '@C_34':1000,
                                },
                    'want':{                            # получаем : количество
                            '@CR_16':1,                 # шестерня
                            },
                    'id':[],                            # каким торговцам, [20200], пусто - все
                    'user':'111222333',                 # какому юзеру
                    # 'full':True,                        # только полные коллекции
                    # 'all':True                          # все, кроме баксовых
                    }
        else: return {'give_coll':{}, 'want':{}, 'id':[], 'user':'', 'full':False, 'all':False}
