#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import re
import time
import shutil
from ctypes import windll

stdout_handle = windll.kernel32.GetStdHandle(-11)
SetConsoleTextAttribute = windll.kernel32.SetConsoleTextAttribute

class ChangeMega():
    def __init__(self, test):
        self.test = test
        self.filename = u'_mega_options.py'
        self.all_text = u''
        self.data = {}
        # current = sys.argv[1]
        # print 'current', current
        print os.getcwd()
        # print
        # print u'обновлялка меги'
        pass

    def run(self):
        if not os.path.isdir('statistics\\Akk_stat'): os.makedirs('statistics\\Akk_stat')

        # список файлов в директории и поддиректориях
        for (dirname, dirs, files) in os.walk('statistics'):
            print u'Перекидываем'
            print u'Папки:'
            print dirs
            print u'\nФайлы:'
            print files
            
            for _dir in dirs:
                if _dir == 'Akk_stat': continue
                # os.makedirs('statistics\\Akk_stat\\' + _dir)
                shutil.move('statistics\\' + _dir, 'statistics\\Akk_stat')

            for file in files:
                if file == 'report_form.py' or file[:8] == 'storage_':
                    continue
                shutil.move('statistics\\' + file, 'statistics\\Akk_stat')
            break

        self.all_text = self.read_file()
        if not self.data:
            self.main_parsing()
        self.disassembly_defs()

        # ------------------  добавляем  defs  ------------------
        if 1:
            _find = None
            for i in range(len(self.defs)):
                if '    def get_path_akkstat' in self.defs[i]:
                    print u'Уже добавлена такая настройка'
                    exit()
            
            _find = None
            for i in range(len(self.defs)):
                if '    def err_log' in self.defs[i]:
                    _find = i + 1
                    break
            if _find:
                text = u"""    # путь к папке статистики и счётчиков акков
    def get_path_akkstat(self):
        return 'statistics\\\\Akk_stat\\\\' # 'C:\\\\Akk_stat\\\\'\n\n"""
                self.defs.insert(_find, text)

        # print u'Всего методов defs', len(self.defs
        # for i in range(len(self.defs)):
            # print i, self.defs[i]
        self.assembly_defs()

        # --------------------  Сохранение  --------------------
        self.saving()


    # -------------------  Тестирование  -------------------
    def testing1(self):
        self.add_import_param('NewClass1', modul='new_module')
        self.add_import_param('NewClass2', modul='cakes_receiver')
        self.add_import_param('NewClass3', modul='craft')
        self.add_import_param('NewClass4', modul='craft')
        print
        self.del_import_param('NoNo', modul='plants')
        self.del_import_param('NewClass1', modul='new_module')
        self.del_import_param('NewClass2', modul='cakes_receiver')
        self.del_import_param('NewClass3', modul='craft')
        self.del_import_param('NewClass4', modul='craft')
        print self.data['part2_import']

    def replace_def_param(self, metod, name, text, before='', after=''):
        pass

    def count_in_def(self, name='', metod=''):
        count = 0
        if not (name and metod): return count, None
        for i in range(len(self.defs)):
            if '    def ' + metod in self.defs[i]:
                # count = self.count_find(name, self.defs[i])
                count = self.defs[i].count(name)
                return count, i

    def add_def_param(self, metod, name, text, before='', after=''):
        if not (metod and name and text) or not (before or after): return
        for i in range(len(self.defs)):
            if '    def ' + metod in self.defs[i]:
                if self.count_find(name, self.defs[i]):
                    # print u'такой параметр уже есть'
                    return
                if before:
                    # sh = re.compile('('+before+')')
                    # self.defs[i] = sh.sub(text + r'\1', self.defs[i]) 
                    self.defs[i], n = re.subn('('+before+')', text + r'\1', self.defs[i]) 
                    # _ff =  re.search('('+before+')', self.defs[i])
                    # print _ff.group()
                elif after:
                    sh = re.compile('('+after+')')
                    # _ff = re.search('('+after+')', self.defs[i])
                    # print _ff.group(), '***', _ff.start(), _ff.end()
                    self.defs[i] = sh.sub(r'\1' + text, self.defs[i])
                # print 'n = ', n
                # print self.defs[i]
                break

    def disassembly_defs(self):
        sh = re.compile(r'((?:#.+\n)?    #.+\n    def .+\(self\):\s*)')
        sdefs = sh.split(self.data['part4_defs'])
        self.defs = []
        for i in range(1, len(sdefs), 2):
            self.defs.append(sdefs[i] + sdefs[i+1])

    def assembly_defs(self):
        self.data['part4_defs'] = ''.join(self.defs)

    def add_import_param(self, param, modul=''):
        if not modul or not param: return
        sh = re.compile(r'    .*?from game_actors_and_handlers(?:.+\\\n)*.+\n')
        modules = sh.findall(self.data['part2_import'])
        # for i in range(len(modules)):
            # print i, modules[i]
        for i in range(len(modules)):
            if 'from game_actors_and_handlers.' + modul + ' import ' in modules[i]:
                if not ((' ' + param + '\n' in modules[i]) or (' ' + param + ', ' in modules[i])):
                    modules[i] = modules[i][:-1] + ', ' + param + '\n'
                break
        else:
            modules.append('    from game_actors_and_handlers.' + modul + ' import ' + param + '\n')
        self.data['part2_import'] = ''.join(modules)

    def del_import_param(self, param, modul=''):
        if not modul or not param: return
        sh = re.compile(r'    .*?from game_actors_and_handlers(?:.+\\\n)*.+\n')
        modules = sh.findall(self.data['part2_import'])
        for i in range(len(modules)):
            name = '    from game_actors_and_handlers.' + modul + ' import'
            if name in modules[i]:
                if ' ' + param not in modules[i]:
                    # print u'В модуле', modul, u'нет класса', param
                    return
                actors = modules[i][len(name):]
                if ',' not in actors:
                    # print u'В модуле', modul, u'параметр', param, u'был последний'
                    modules.remove(modules[i])
                    break
                sh1 = re.compile(r'(,\s*' + param + ')')
                sh2 = re.compile(r'(' + param + ',?\s*)') # (?:\n)?
                __find1 = sh1.search(actors)
                __find2 = sh2.search(actors)
                if __find1:
                    # print '__find1', __find1.group()
                    actors = sh1.sub('', actors)
                    # print actors
                elif __find2:
                    # print '__find2', __find2.group()
                    actors = sh2.sub('', actors)
                    # print actors
                else:
                    print 'ERROR'
                    return
                modules[i] = name + actors
                break
        else:
            # print u'нет модуля', modul
            return
        self.data['part2_import'] = ''.join(modules)

    def main_parsing(self):
        rp1 = re.compile(r'if True:.*\n')
        part1 = self._find(rp1, True)
        self.data['part1'] = self.all_text[:part1.end()]

        rp2 = re.compile(r'\nstdout_handle =')
        part2 = self._find(rp2, True)
        self.data['part2_import'] = self.all_text[part1.end():part2.start()]
        # print self.all_text[part2.start():part2.start()+500]
        # print self.all_text[part1.start():part1.end()]
        # print '-----------', len(self.all_text[part1.start():part1.end()])
        # print self.all_text[part2.start():part2.end()]

        rp3 = re.compile(u'(?:#.+\n)?    #.+\n    def actor_options\(self\):')
        part3 = self._find(rp3, True)
        self.data['part3_init'] = self.all_text[part2.start():part3.start()]

        rp4 = re.compile(u'\n+    # Сюда добавляем настройки для новых модулей !!!')
        part4 = self._find(rp4, True)
        self.data['part4_defs'] = self.all_text[part3.start():part4.start()]
        # print self.data['part4_defs']
        
        self.data['part5_end'] = self.all_text[part4.start():]

    def count_find(self, sh, all):
        # csh = re.compile(sh) 
        # f = csh.findall(all)
        f = re.findall(sh, all)
        return len(f)

    def global_replace(self, sh, new):
        self.all_text = re.sub(sh, new, self.all_text)

    def _replace(self, sh, new, all):
        new_all = re.sub(sh, new, all)
        return new_all

    def _find(self, sh, mandatory):
        m = sh.search(self.all_text)
        if mandatory and not m:
            self.exit_error(u'Не найден обязательный шаблон')
        return m

    def read_file(self):
        if not os.path.isfile(self.filename):
            self.exit_error(u'Не найден файл^15_' + self.filename)
        with open(self.filename, 'r') as fr:
            return fr.read().decode('utf-8')

    def saving(self):
        if self.data:
            out = self.data['part1'] +\
                    self.data['part2_import'] +\
                    self.data['part3_init'] +\
                    self.data['part4_defs'] +\
                    self.data['part5_end']
            self.save_file(out)
        else:
            self.save_file(self.all_text)

    def save_file(self, text):
        with open(self.filename, 'w') as f:
            f.write(text.encode('utf-8'))

    def exit_ok(self):
        if self.test:
            raw_input('---------------   END   ---------------')
        sys.exit(0)

    def exit_error(self, text):
        self.cprint(u'12%s' % text)
        time.sleep(3)
        if self.test:
            raw_input('---------------   END   ---------------')
        sys.exit(1)

    def cprint(self, cstr):
        clst = cstr.split('^')
        color = 0x0001
        for cstr in clst:
            dglen = re.search("\D", cstr).start()
            color = int(cstr[:dglen])
            text = cstr[dglen:]
            if text[:1] == "_": text = text[1:]
            SetConsoleTextAttribute(stdout_handle, color | 0x0070) #78
            print text.replace(u'\u0456', u'i').encode("cp866", "ignore"),
        #sys.stdout.flush()
        print ""
        SetConsoleTextAttribute(stdout_handle, 0x0001 | 0x0070)


if __name__ == '__main__':
    test = True
    change_mega = ChangeMega(test)
    change_mega.run()

# chdir /D D:\Яндекс Диск\_Update_ZomBot
# python change_mega.py

# ------------------------------------------------------------------------------
# Для консоли

'''
m.group()  # Вернуть строку, сошедшуюся с регулярным выражением
m.start()  # Вернуть позицию начала совпадения
m.end()    # Вернуть позицию конца совпадения
m.span()   # Вернуть кортеж (start, end) позиций совпадения
'''




"""
import os, re

def pred_start():
    os.chdir('D:\\Яндекс Диск\\_Update_ZomBot')
    print os.getcwd()

    filename = '_mega_options.py'
    with open(filename, 'r') as fr:
        all_text = fr.read().decode('utf-8')
    return all_text

def _find(sh, mandatory):
    m = sh.search(all_text)
    if mandatory and not m:
        print 'Не найден обязательный шаблон'
    return m

def main_parsing(all_text):
    data = {}
    rp1 = re.compile('if True:\n')    
    part1 = _find(rp1, True)
    data['part1'] = all_text[:part1.end()]

    rp2 = re.compile('\nstdout_handle =')
    part2 = _find(rp2, True)
    data['part2_import'] = all_text[part1.end():part2.start()]

    rp3 = re.compile('    def actor_options\(self\):')
    part3 = _find(rp3, True)
    data['part3_init'] = all_text[part2.start():part3.start()]

    rp4 = re.compile('\n\n\n    # Сюда добавляем настройки для новых модулей !!!'.decode('cp1251'))
    part4 = _find(rp4, True)
    data['part4_defs'] = all_text[part3.start():part4.start()]

    data['part5_end'] = all_text[part4.start():]
    return data

def add_import_param(data, param, modul='craft'):
    sh = re.compile(r'    .*?from game_actors_and_handlers(?:.+\\\n)*.+\n')
    modules = sh.findall(data['part2_import'])
    for i in range(len(modules)):
        if 'from game_actors_and_handlers.' + modul + ' import ' in modules[i]:
            if not ((' ' + param + '\n' in modules[i]) or (' ' + param + ', ' in modules[i])):
                modules[i] = modules[i][:-2] + ', ' + param + '\n'
            break
    else:
        modules.append('    from game_actors_and_handlers.' + modul + ' import ' + param + '\n')
    data['part2_import'] = ''.join(modules)

def save_file(text):
    filename = u'_mega_options.py'
    with open(filename, 'w') as f:
        f.write(text.encode('utf-8'))

def count_find(sh, all):
    f = re.findall(sh, all)
    return len(f)

def global_replace(sh, new):
    global all_text
    all_text = re.sub(sh, new, all_text)

def _replace(sh, new, all):
    new_all = re.sub(sh, new, all)
    return new_all


all_text = pred_start()

count_find('ExchangeSuperglue', all_text)
count_find('ExchangeKley', all_text)
global_replace('ExchangeSuperglue', 'ExchangeKley')

data = main_parsing(all_text)
add_import_param(data, 'ExchangeSuperglue', modul='craft')

modules = re.findall(r'    .*?from game_actors_and_handlers.(.*?) import ', data['part2_import'])
len(re.findall(r'    .*?from game_actors_and_handlers.+\n', data['part2_import']))
sh = re.compile(r'    .*?from game_actors_and_handlers.+\n')
modules = re.split(r'\w+\n', data['part2_import'])

modules = re.findall(r'    .*?from game(?:.*\\\n)*.+\n', data['part2_import'])
add_import_param(modules, 'ExchangeSuperglue', modul='craft')
print modules[2]
def add_import_param(modules, param, modul='craft'):
    for i in range(len(modules)):
        if 'from game_actors_and_handlers.' + modul + ' import ' in modules[i]:
            if not ((' ' + param + '\n' in modules[i]) or (' ' + param + ', ' in modules[i])):
                modules[i] = modules[i][:-2] + ', ' + param + '\n'
            break
    else:
        modules.append('    from game_actors_and_handlers.' + modul + ' import ' + param + '\n')



save_file(all_text)
"""

# s = unicode('Привет', 'cp1251')
# s = 'Привет'.decode('cp1251')